﻿using Microsoft.Extensions.DependencyInjection;

namespace DataAccess.Repositories
{
    public static class RegisterRepositories
    {
        public static void AddRepositoryServices(this IServiceCollection services)
        {
            services.AddScoped<IRequestRepository, RequestRepository>();
            services.AddScoped<IPhotoRepository, PhotoRepository>();
            services.AddScoped<IAlbumRepository, AlbumRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IUserRoleRepository, UserRoleRepository>();
            services.AddScoped<IRoleRepository, RoleRepository>();
            services.AddScoped<ILoggingRepository, LoggingRepository>();
            services.AddScoped<IFileRepository, FileRepository>();
            services.AddScoped<ISettingRepository, SettingRepository>();
        }
    }
}
