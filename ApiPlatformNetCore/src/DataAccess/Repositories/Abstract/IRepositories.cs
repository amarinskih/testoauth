﻿using DataAccess.Entities;
using DataAccess.Entities.File;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface IAlbumRepository : IEntityBaseRepository<Album> { }

    public interface IRequestRepository : IEntityBaseRepository<Request> { }

    public interface ILoggingRepository : IEntityBaseRepository<Error> { }

    public interface IPhotoRepository : IEntityBaseRepository<Photo> { }

    public interface IRoleRepository : IEntityBaseRepository<Role> { }

    public interface IUserRepository : IEntityBaseRepository<User>
    {
        User GetSingleByUsername(string username);
        IEnumerable<Role> GetUserRoles(string name);
        Task<IQueryable<User>> GetUserRolesAsync();
        void UpdateRoles(int id, int[] roleId);
    }

    public interface IUserRoleRepository : IEntityBaseRepository<UserRole> { }

    public interface IFileRepository
    {
        IEnumerable<FileDescriptionShort> AddFileDescriptions(FileResult fileResult, out List<FileDescription> files);

        IEnumerable<FileDescriptionShort> GetAllFiles();

        FileDescription GetFileDescription(int id);
    }

    public interface ISettingRepository : IEntityBaseRepository<Setting> {
        Task<string> GetOptions();
        void UpdateOptions(string value);
    }

}
