﻿using DataAccess.Entities;

namespace DataAccess.Repositories
{
    public class RoleRepository : EntityBaseRepository<Role>, IRoleRepository
    {
        public RoleRepository(DataContext context)
            : base(context)
        { }
    }
}
