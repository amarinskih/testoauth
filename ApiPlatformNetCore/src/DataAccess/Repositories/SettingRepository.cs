﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class SettingRepository : EntityBaseRepository<Setting>, ISettingRepository
    {
        public SettingRepository(DataContext context)
            : base(context)
        {
        }
        public async Task<string> GetOptions() {
            var o = await this.GetSingleAsync(e=>e.JsonOptions!=null);
            if (o == null) return "";
            return o.JsonOptions;
        }
        public void UpdateOptions(string value) {
            var o = this.GetSingle();
            if (o == null) this.Add(new Setting { JsonOptions = value });
            else {
                o.JsonOptions = value;
                this.Edit(o);
            }
            this.Commit();
        }
    }
}
