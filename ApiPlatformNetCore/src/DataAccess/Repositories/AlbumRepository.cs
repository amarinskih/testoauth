﻿using DataAccess.Entities;

namespace DataAccess.Repositories
{
    public class AlbumRepository : EntityBaseRepository<Album>, IAlbumRepository
    {
        public AlbumRepository(DataContext context) : base(context)
        { }
    }
}
