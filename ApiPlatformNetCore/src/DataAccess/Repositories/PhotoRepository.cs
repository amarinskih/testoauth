﻿using DataAccess.Entities;

namespace DataAccess.Repositories
{
    public class PhotoRepository : EntityBaseRepository<Photo>, IPhotoRepository
    {
        public PhotoRepository(DataContext context)
            : base(context)
        { }
    }
}
