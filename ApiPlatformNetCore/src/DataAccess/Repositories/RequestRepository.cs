﻿using DataAccess.Entities;

namespace DataAccess.Repositories
{
    public class RequestRepository : EntityBaseRepository<Request>, IRequestRepository
    {
        public RequestRepository(DataContext context) : base(context)
        { }
    }
}
