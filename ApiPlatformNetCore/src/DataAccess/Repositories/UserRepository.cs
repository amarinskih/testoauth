﻿using DataAccess.Entities;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using System.Linq;

namespace DataAccess.Repositories
{
    public class UserRepository : EntityBaseRepository<User>, IUserRepository
    {
        IRoleRepository _roleReposistory;
        IUserRoleRepository _userRoleReposistory;

        public UserRepository(DataContext context, IRoleRepository roleReposistory, IUserRoleRepository userRoleReposistory)
            : base(context)
        {
            _roleReposistory = roleReposistory;
            _userRoleReposistory = userRoleReposistory;
        }

        public User GetSingleByUsername(string username)
        {
            return this.GetSingle(x => x.UserName == username);
        }

        public IEnumerable<Role> GetUserRoles(string name)
        {
            List<Role> _roles = null;

            User _user = this.GetSingle(u => u.UserName == name, u => u.UserRoles);
            if(_user != null)
            {
                _roles = new List<Role>();
                foreach (var _userRole in _user.UserRoles)
                    _roles.Add(_roleReposistory.GetSingle(_userRole.RoleId));
            }

            return _roles;
        }

        public async Task<IQueryable<User>> GetUserRolesAsync()
        {
            var _users = base.AllIncluding(param=>param.UserRoles);
            await _userRoleReposistory.AllIncludingAsync(e => e.Role);
            return _users;
        }

        public void UpdateRoles(int id, int[] roleId) {
            var _roles = _userRoleReposistory.FindBy(e => e.UserId == id);
            foreach (var d in _roles.Where(e => !roleId.Contains(e.RoleId))) {
                _userRoleReposistory.Delete(d);
            }
            foreach (var d in roleId.Where(e => !_roles.Select(s=>s.RoleId).Contains(e)))
            {
                _userRoleReposistory.Add(new UserRole { RoleId=d, UserId=id});
            }
            _userRoleReposistory.Commit();
        }
    }
}
