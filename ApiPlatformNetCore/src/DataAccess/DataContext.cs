﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using DataAccess.Entities;
using DataAccess.Entities.File;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace DataAccess
{
    public class DataContext : IdentityDbContext<User, Role, int>
    {
        public DbSet<Request> Requests { get; set; }
        public DbSet<Photo> Photos { get; set; }
        public DbSet<Album> Albums { get; set; }
        public DbSet<Error> Errors { get; set; }
        public DbSet<FileDescription> FileDescriptions { get; set; }
        public DbSet<Setting> Settings { get; set; }

        public DataContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            foreach (var entity in modelBuilder.Model.GetEntityTypes())
            {
                entity.Relational().TableName = entity.DisplayName();
            }

            // Photos
            modelBuilder.Entity<Photo>().Property(p => p.Title).HasMaxLength(100);
            modelBuilder.Entity<Photo>().Property(p => p.AlbumId).IsRequired();

            // Album
            modelBuilder.Entity<Album>().Property(a => a.Title).HasMaxLength(100);
            modelBuilder.Entity<Album>().Property(a => a.Description).HasMaxLength(500);
            modelBuilder.Entity<Album>().HasMany(a => a.Photos).WithOne(p => p.Album);

            //// User
            //modelBuilder.Entity<User>().Property(u => u.Username).IsRequired().HasMaxLength(100);
            //modelBuilder.Entity<User>().Property(u => u.Email).IsRequired().HasMaxLength(200);
            //modelBuilder.Entity<User>().Property(u => u.HashedPassword).IsRequired().HasMaxLength(200);
            //modelBuilder.Entity<User>().Property(u => u.Salt).IsRequired().HasMaxLength(200);
            

            //// UserRole
            //modelBuilder.Entity<UserRole>().Property(ur => ur.UserId).IsRequired();
            //modelBuilder.Entity<UserRole>().Property(ur => ur.RoleId).IsRequired();

            //// Role
            //modelBuilder.Entity<Role>().Property(r => r.Name).IsRequired().HasMaxLength(50);

            //modelBuilder.Entity<Role>().HasMany(e => e.UserRoles).WithOne(e => e.Role).HasForeignKey(e => e.RoleId);

            modelBuilder.Entity<FileDescription>().HasKey(m => m.Id);
        }
    }
}