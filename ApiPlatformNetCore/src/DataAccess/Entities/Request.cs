﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class Request : IEntityBase
    {
        public Request()
        {

        }
        public int Id { get; set; }
        public string FIO { get; set; }
        public string Phone { get; set; }
        public string Comment { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
        public DateTime DateCreated { get; set; }
        public string House { get; set; }
        public string Flat { get; set; }
        public StatusRequest StatusRequest { get; set; }
    }

    public enum StatusRequest
    {
        [Display(Name = "Новая")]
        New = 1,
        [Display(Name = "В работе")]
        Work = 2,
        [Display(Name = "Выполнена")]
        Complete = 3
    }
}
