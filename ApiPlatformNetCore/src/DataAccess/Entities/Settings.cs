﻿namespace DataAccess.Entities
{
    public class Setting:IEntityBase
    {
        public int Id { get; set; }
        public string JsonOptions { get; set; }
    }
}
