﻿import { NgModule, ModuleWithProviders, Optional, SkipSelf } from '@angular/core';
import { User } from './domain/user';
import { LocalStorage } from './services/localStorage';
import { UserStoreService } from './services/userStoreService';
import { ApplicationRequestService } from './services/applicationRequestService';

@NgModule({
    imports: [

    ],
    providers: [UserStoreService, ApplicationRequestService]
})
export class CoreModule {
    constructor( @Optional() @SkipSelf() parentModule: CoreModule) {
        if (parentModule) {
            throw new Error(
                'CoreModule is already loaded. Import it in the AppModule only');
        }
    }

    static forRoot(): ModuleWithProviders {
        return {
            ngModule: CoreModule,
            providers: [
                { provide: UserStoreService, useValue: new UserStoreService() }
            ]
        };
    }

}

export * from './domain/user';
export * from './services/localStorage';
export * from './services/userStoreService';
export * from './services/applicationRequestService';
export * from './contracts/requestModel';
export * from './domain/responseModel';
