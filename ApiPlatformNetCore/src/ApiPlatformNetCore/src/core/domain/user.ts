﻿export class User {
    public AccessToken: string;
    public RefreshToken: string;
    public UserName: string;
    public Email: string;
}