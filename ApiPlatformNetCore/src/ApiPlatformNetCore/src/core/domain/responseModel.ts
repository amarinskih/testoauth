﻿export class ApiError {
    Message: string;
    Code: number
}


export class ResponseModel {
    Succeeded: boolean;
    Message: ApiError;
    StatusCode: number;
}

export class ResponseResultModel<T> extends ResponseModel {
    Result: T;
}

export interface IResponse {
    Succeeded: boolean;
    Message: ApiError;
}

export interface IResultResponse extends IResponse {
    Result: any
}