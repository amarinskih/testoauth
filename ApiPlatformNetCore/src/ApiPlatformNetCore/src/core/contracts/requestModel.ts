﻿export interface RequestModel {
    toDTO(): any;
}

export class BaseRequestModel implements RequestModel {
    toDTO() {
        return this;
    }
}