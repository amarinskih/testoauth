﻿
export enum StorageMode {
    Local = 0,
    Session = 1,
    Memory = 2
}

const applicationPrefix: string = "myApp_storage";


export class LocalStorage {
    private _storage: Storage;

    constructor(mode: StorageMode) {
        switch (mode) {
            case StorageMode.Local:
                this._storage = window.localStorage;
                break;
            case StorageMode.Session:
                this._storage = window.sessionStorage;
                break;
            case StorageMode.Memory:
                break;
        }
    }

    setValue(key: string, value: any): boolean {
        if (!key)
            throw new Error("Key param is required");
        key = applicationPrefix + "__" + key;
        let jsonValue = "";
        try {
            jsonValue = JSON.stringify(value);
            this._storage[key] = jsonValue;
            return true;
        } catch (e) {
            console.debug(e);
            return false;
        }
    }

    getValue<T>(key: string):T {
        if (!key)
            throw new Error("Key param is required");
        key = applicationPrefix + "__" + key;
        let storedValue = this._storage[key];
        try {
            let parsedValue = JSON.parse(storedValue);
            return <T>parsedValue;
        } catch (e) {
            console.debug(e);
            return undefined;
        }
    }



    private _activeCachers: { [key: string]: UnsubscribeFunction } = {};

    registerCacher(key: string, getter: GetterFunction, interval: number = 1000) {
        if (!key) throw new Error("Key param is required");
        if (!getter) throw new Error("Getter function is required");
        key = applicationPrefix + "__" + key;
        if (this._activeCachers[key]) {
            this._activeCachers[key]();
        }
        let intervalId = window.setInterval(() => {
            let value = getter();
            this.setValue(key, value);
        });
        return this._activeCachers[key] = () => {
            window.clearInterval(intervalId);
            delete this._activeCachers[key];
        }
    }

    reset(key: string): void {
        if (!key) throw new Error("Key param is required");
        key = applicationPrefix + "__" + key;
        if (this._activeCachers[key]) {
            this._activeCachers[key]();
        }
        delete this._storage[key];
    }

    resetAll(): void {
        for (let key in this._activeCachers) {
            this._activeCachers[key]();
        }
        for (let key in this._storage) {
            if (key.indexOf(applicationPrefix) == 0)
                delete this._storage[key];
        }
    }

}

export interface GetterFunction {
    (): any
}

export interface UnsubscribeFunction {
    (): void
}