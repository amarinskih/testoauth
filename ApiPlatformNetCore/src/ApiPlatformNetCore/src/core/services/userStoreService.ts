﻿import { Injectable } from '@angular/core';
import { User } from '../domain/user';
import { LocalStorage, StorageMode } from './localStorage';

const userStorageKey: string = "application.user";

@Injectable()
export class UserStoreService {

    private currentUser: User;
    private _storageService: LocalStorage = new LocalStorage(StorageMode.Local); 

    constructor() {
        this.restoreUser();
    }

    private restoreUser() {
        this.currentUser = <User>this._storageService.getValue(userStorageKey);
    }

    setUser(user: User) {
        this.currentUser = user;
        this._storageService.setValue(userStorageKey, user);
    }

    getUser(): User {
        return this.currentUser;
    }

    resetUser() {
        this.currentUser = null;
        this._storageService.resetAll();
    }
}