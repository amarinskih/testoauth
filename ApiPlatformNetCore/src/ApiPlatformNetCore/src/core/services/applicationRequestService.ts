﻿import { Http, Response, RequestOptionsArgs, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RequestModel } from '../contracts/requestModel';
import { ResponseModel, ResponseResultModel, IResponse, IResultResponse } from '../domain/responseModel';


import { UserStoreService } from './userStoreService'; 

@Injectable()
export class ApplicationRequestService {

    constructor(public http: Http, private _userStore: UserStoreService) {
    }

    get(url: string, query?: Object | string, requestOptions?: RequestOptionsArgs) {
        requestOptions = this.applyAuthHeaders(requestOptions);
        if (query) {
            if (typeof query == "string") {
                url += "?" + query;
            } else if (typeof query == "object") {
                url += "?" + this.urlEncode(query);
            }
        }
        return this.http.get(url, requestOptions)
            .map(response => (<Response>response));    
    }

    post(url: string, data?: any, contentType: ContentType = ContentType.JSON, requestOptions?: RequestOptionsArgs, isAnnonimous: boolean = false): Observable<Response> {
        if (!isAnnonimous)
            requestOptions = this.applyAuthHeaders(requestOptions);
        requestOptions = this.addContentTypeHeaders(contentType, requestOptions);
        let requestObject: any = ('toDTO' in data) ? (<RequestModel>data).toDTO() : data; 
        let request = data ? this.prepareData(requestObject, contentType) : null;
        return this.http.post(url, request, requestOptions).map(response => (<Response> response));
    }

    delete(url: string, requestOptions?: RequestOptionsArgs, isAnnonimous: boolean = false) {
        if (!isAnnonimous)
            requestOptions = this.applyAuthHeaders(requestOptions);
        return this.http.delete(url, requestOptions);
    }

    put(url: string, data: any, contentType: ContentType = ContentType.JSON, requestOptions?: RequestOptionsArgs, isAnnonimous: boolean = false) {
        if (!isAnnonimous)
            requestOptions = this.applyAuthHeaders(requestOptions);
        requestOptions = this.addContentTypeHeaders(contentType, requestOptions);
        let requestObject: any = ('toDTO' in data) ? (<RequestModel>data).toDTO() : data; 
        let request = data ? this.prepareData(requestObject, contentType) : null;
        return this.http.post(url, request, requestOptions).map(response => (<Response>response));
    }

    applyAuthHeaders(requestOptions?: RequestOptionsArgs) {
        requestOptions = requestOptions || {};
        var user = this._userStore.getUser();
        if (user) {
            if (requestOptions.headers != null)
                requestOptions.headers.append("Authorization", "Bearer " + user.AccessToken);
            else
                requestOptions.headers = new Headers({ "Authorization": "Bearer " + user.AccessToken});
        }
        return requestOptions;
    }

    addContentTypeHeaders(contentType: ContentType, requestOptions: RequestOptionsArgs) {
        if (contentType == ContentType.Raw)
            return requestOptions;

        requestOptions = requestOptions || {};
        let contentTypeString = "";
        switch (contentType) {
            case ContentType.JSON:
                contentTypeString = "application/json";
                break;
            case ContentType.FormUrlEncoded:
                contentTypeString = "application/www-form-url-encoded";
                break;
        }
        if (requestOptions.headers != null)
            requestOptions.headers.append("Content-Type", contentTypeString);
        else
            requestOptions.headers = new Headers({ "Content-Type": contentTypeString });
        return requestOptions;

    }

    prepareData(data: any, contentType: ContentType): any {
        if (!data) return data;
        let preparedData: any;
        try {
            switch (contentType) {
                case ContentType.JSON:
                    preparedData = JSON.stringify(data);
                    break;
                case ContentType.FormUrlEncoded:
                    preparedData = this.urlEncode(data);
                    break;
                case ContentType.Raw:
                    preparedData = data;
                    break;
            }
        } catch (e) {
            console.debug(e);
        }
        return preparedData;
    }

    urlEncode(obj: Object) {
        var str = [];
        for (var p in obj)
            if (obj.hasOwnProperty(p)) {
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
        return str.join("&");
    }



    mapResponseModel(response: Response): ResponseModel {
        let data = this.parseResponse(response);
        let responseModel = new ResponseModel();
        responseModel.StatusCode = response.status;
        responseModel.Succeeded = data.Succeeded;
        responseModel.Message = data.Message;
        return responseModel;
    }

    mapResultResponseModel<T>(response: Response): ResponseResultModel<T> {
        let data = this.parseResponse(response);
        let responseModel = new ResponseResultModel<T>();
        responseModel.StatusCode = response.status;
        responseModel.Succeeded = data.Succeeded;
        responseModel.Message = data.Message;
        responseModel.Result = <T>data.Result;
        return responseModel;
    }

    parseResponse(response: Response): IResultResponse {
        let data: any;
        try {
            data = response.json();
        } catch (e) {
            console.log(e);
        }
        return <IResultResponse>data;
    }
}

export enum ContentType {
    Raw = 0,
    JSON = 1,
    FormUrlEncoded = 2
}