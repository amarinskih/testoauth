﻿import { ModuleWithProviders } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { Home } from './components/home';

export const appRoutes: { [key: string]: Route } = {
    '': { path: '', redirectTo: 'home', pathMatch: 'full' },
    'home': { path: 'home', component: Home },
    'request': {
        path: 'request', loadChildren: () => new Promise(resolve => {
            (require as any).ensure([], (require: any) => {
                resolve(require('./components/request/request.module').RequestModule);
            })
        })
    },
    'account': {
        path: 'account', loadChildren: () => new Promise(resolve => {
            (require as any).ensure([], (require: any) => {
                resolve(require('./components/account/account.module').AccountModule);
            })
        })
    },
    'photo': {
        path: 'photo',
        loadChildren: () => new Promise(resolve => {
            (require as any).ensure([], (require: any) => {
                resolve(require('./components/photo/photo.module').PhotoModule);
            })
        })
    }
};

export const appRoutingProviders: any[] = [

];

export const routing: ModuleWithProviders = RouterModule.forRoot(Object.keys(appRoutes).map(key => appRoutes[key]));