﻿import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';

import { accountRoutes } from './account.routes';
import { AccountService } from '../../core/services/accountService';
import { RegisterModel } from './account.models';
//import { NotificationService } from '../../core/services/notificationService';

@Component({
    selector: 'register',
    templateUrl: './register.html',
})
export class Register implements OnInit {

    private routes = accountRoutes;
    private _newUser: RegisterModel;

    constructor(public accountService: AccountService,
        public router: Router) { }

    ngOnInit() {
        this._newUser = new RegisterModel();
        this.routes = accountRoutes;
    }

    register() {
        this.accountService.register(this._newUser).subscribe(response => {
            if (response.Succeeded) {
                if (response.Result.NeedConfirmation) {
                    alert('Для подтверждения регистрации перейдите по ссылке, указанной в письме, отправленном вам на email');
                } else {
                    this.router.navigate(['/home']);
                }
            } else {
                alert(response.Message.Message);
            }
        })
    }
}