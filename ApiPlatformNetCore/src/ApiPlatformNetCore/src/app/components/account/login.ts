﻿import { Component, OnInit } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { appRoutes } from '../../app.routes';

import { LoginModel } from './account.models';
import { AccountService } from '../../core/services/accountService'; 
import { ApplicationRequestService } from '../../../core/services/applicationRequestService';

@Component({
    selector: 'login',
    templateUrl: './login.html'
})
export class Login implements OnInit {
    private _user: LoginModel;

    constructor(public accountService: AccountService,
        public albumsService: ApplicationRequestService,
        public router: Router) { }

    ngOnInit() {
        this._user = new LoginModel();
    }

    login(): void {

        this.accountService.login(this._user)
            .subscribe(
            res => {
                if (res.Succeeded) {
                    this.router.navigate(['/home']);
                } else {
                    alert(res.Message.Message);
                }
            },
            error => console.error('Error: ' + error),
            () => { });
    }
}