﻿import { ModuleWithProviders } from '@angular/core';
import { Route, RouterModule } from '@angular/router';

import { Login } from './login';
import { Register } from './register';
import { ResetPassword } from './resetPassword';
import { RecoveryPassword } from './recoveryPassword';
import { ConfirmEmail } from './confirmEmail'

export const accountRoutes: { [key: string]: Route } = {
    '': { path: '', redirectTo: 'login', pathMatch: 'full' },
    'login': { path: 'login', component: Login },
    'register': { path: 'register', component: Register },
    'reset_password': { path: 'resetpassword/:id', component: ResetPassword },
    'recovery_password': { path: 'recoverypassword', component: RecoveryPassword },
    'confirm_email': { path: 'confirmemail', component: ConfirmEmail}
};

export const appRoutingProviders: any[] = [

];

export const routing: ModuleWithProviders = RouterModule.forChild(Object.keys(accountRoutes).map(key => accountRoutes[key]));