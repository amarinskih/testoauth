﻿import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { accountRoutes } from './account.routes';

//import { NotificationService } from '../../core/services/notificationService';
import { RestorePasswordModel } from './account.models';
import { AccountService } from '../../core/services/accountService';


@Component({
    selector: 'resetpassword',
    templateUrl: './resetPassword.html'
})
export class ResetPassword implements OnInit {
    private routes = accountRoutes;
    private _newPassword: RestorePasswordModel;
    private PasswordConfirm: string = "";

    constructor(public _accountService: AccountService,
        public router: Router,
        public routeParam: ActivatedRoute) { }

    ngOnInit() {
        this._newPassword = new RestorePasswordModel(this.routeParam.snapshot.params['id'], '', this.routeParam.snapshot.queryParams['token']);
        this.routes = accountRoutes;
    }

    resetPassword(): void {
        this._accountService.resetPassword(this._newPassword).subscribe(result => {
            if (result.Succeeded) {
                this.router.navigate(['/account/login']);
            } else {
                console.error("Error " + result.Message.Message);
            }
        }, error => console.error("Error " + error));
    }
}