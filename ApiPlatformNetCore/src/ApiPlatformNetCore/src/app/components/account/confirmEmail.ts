﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AccountService } from '../../core/services/accountService'

@Component({
    selector: 'confirmemail',
    templateUrl: './confirmEmail.html'
})
export class ConfirmEmail implements OnInit {

    constructor(private _router: Router,
        private _accountService: AccountService,
        private _route: ActivatedRoute
    ) { }

    ngOnInit() {
        let code = this._route.snapshot.queryParams["code"];
        let userId = this._route.snapshot.queryParams["userId"];
        if (!code || !userId) {
            this._router.navigate['/home'];
            return;
        }
        this._accountService.confirmEmail(userId, code).subscribe(
            response => {
                if (response.Succeeded) {
                    this._accountService.setUser(response.Result);
                }
            },
            error => {
                console.log(error);
            }, () => {
                this._router.navigate['/home'];
            }
        )
    }
}