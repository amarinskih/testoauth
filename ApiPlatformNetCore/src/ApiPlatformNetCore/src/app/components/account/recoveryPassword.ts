﻿import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { accountRoutes } from './account.routes';

import { AccountService } from '../../core/services/accountService';


@Component({
    selector: 'recoverypassword',
    templateUrl: './recoveryPassword.html'
})
export class RecoveryPassword implements OnInit {
    private routes = accountRoutes;
    Email: string = "";

    constructor(private _accountService: AccountService,
        public router: Router) { }

    ngOnInit() {
        this.routes = accountRoutes;
    }

    resetPassword(): void {
        this._accountService.recoveryPassword(this.Email).subscribe(response => {
            if (response.Succeeded) {
                this.router.navigate(['/account/login']);
            } else {
                console.error('Error: ' + response.Message.Message);
            }
        }, error => console.error('Error: ' + error));
    }
}