﻿import { RequestModel, BaseRequestModel, User } from '../../../core/core.module';
//Login
export class LoginModel implements RequestModel {
    username: string;
    password: string;
    rememberMe: boolean;
    toDTO = () => {
        return {
            UserName: this.username,
            Password: this.password,
            RememberMe: this.rememberMe
        };
    }
}
//Register and Confirm Email
export class RegisterModel implements RequestModel {
    Username: string;
    Email: string;
    Password: string;
    toDTO = () => {
        return {
            UserName: this.Username,
            Password: this.Password,
            Email: this.Email,
        }
    }

}

export class RegisterResponse {
    NeedConfirmation: boolean;
    User: User
}

export class ConfirmEmailRequest extends BaseRequestModel {
    constructor(
        public UserId: string,
        public Code: string)
    {
        super()
    }
}

//Restore Password
export class RestorePasswordModel extends BaseRequestModel {
    Id: number;
    Password: string;
    Token: string
    constructor(id: number, password: string, token: string) {
        super();
        this.Id = id;
        this.Password = password;
        this.Token = token;
    }
}