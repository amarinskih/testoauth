﻿import { NgModule } from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { routing } from './account.routes';

import { Login } from './login';
import { Register } from './register';
import { ResetPassword } from './resetPassword';
import { RecoveryPassword } from './recoveryPassword';
import { ConfirmEmail } from './confirmEmail';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule,
        routing
    ],
    declarations: [
        Login, Register, ResetPassword, RecoveryPassword, ConfirmEmail
    ],
    providers: [],
})
export class AccountModule { }