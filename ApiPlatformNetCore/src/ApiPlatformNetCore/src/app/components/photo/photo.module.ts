﻿import { NgModule } from '@angular/core';
import { CommonModule }  from '@angular/common';
import { RouterModule } from '@angular/router';

import { FILE_UPLOAD_DIRECTIVES } from 'ng2-file-upload'

import { routing } from './photo.routes';

import { AlbumPhotos } from './albumPhotos';
import { Albums } from './albums';
import { Photos } from './photos';


@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        routing
    ],
    declarations: [
        AlbumPhotos, Albums, Photos, FILE_UPLOAD_DIRECTIVES
    ],
    providers: [],
})
export class PhotoModule { }