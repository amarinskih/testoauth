﻿import { Component, OnInit } from '@angular/core';
import { ActivatedRoute }    from '@angular/router';

import { RouterLink } from '@angular/router';
import { Photo } from '../../core/domain/photo';
import { Paginated } from '../../core/common/paginated';
import { DataService } from '../../core/services/dataService';
import { UtilityService } from '../../core/services/utilityService';
import { NotificationService } from '../../core/services/notificationService';
import { OperationResult } from '../../core/domain/operationResult';
import { FileUploader} from 'ng2-file-upload';


@Component({
    selector: 'album-photo',
    templateUrl: './albumPhotos.html',
})
export class AlbumPhotos extends Paginated implements OnInit {
    private _albumsAPI: string = 'api/albums/';
    private _photosAPI: string = 'api/photos/';
    private _albumId: string;
    private _photos: Array<Photo>;
    private _displayingTotal: number;
    private _albumTitle: string;
    private _file: File;

    public uploader: FileUploader = new FileUploader({ url: 'api/albums/fileuploader/' });

    constructor(public dataService: DataService,
        public utilityService: UtilityService,
        public notificationService: NotificationService,
        public route: ActivatedRoute
    ) {

        super(0, 0, 0);
        this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            if (status == 200 && response.length > 0) {
                Array.prototype.push.apply(this._photos, JSON.parse(response));
                return;
            }
            //var responsePath = JSON.parse(response);
            console.log(item);
            console.log(response);
            console.log(status);
            console.log(headers);
        };
    }

    ngOnInit() {
        this._albumId = this.route.params['id'];
        this._albumsAPI += this._albumId + '/photos/';
        this.dataService.set(this._albumsAPI, 12);
        this.getAlbumPhotos();
        this.uploader.onBuildItemForm = (fileItem: any, form: any) => {
            form.append('id', this._albumId);
            //form.append('name', '');
        };
    }

    getAlbumPhotos(): void {
        this.dataService.get(this._page)
            .subscribe(res => {
                var data: any = res.json();
                this._photos = data.Items;
                this._displayingTotal = this._photos.length;
                this._page = data.Page;
                this._pagesCount = data.TotalPages;
                this._totalCount = data.TotalCount;
                this._albumTitle = data.AlbumTitle;
            },
            error => {
                if (error.status == 401 || error.status == 302) {
                    this.utilityService.navigateToSignIn();
                }

                console.error('Error: ' + error)
            },
            () => console.log(this._photos));
    }

    search(i): void {
        super.search(i);
        this.getAlbumPhotos();
    };

    convertDateTime(date: Date) {
        return this.utilityService.convertDateTime(date);
    }

    delete(photo: Photo) {
        var _removeResult: OperationResult = new OperationResult(false, '');

        this.notificationService.printConfirmationDialog('Вы уверены, что хотите удалить фотографию?',
            () => {
                this.dataService.deleteResource(this._photosAPI + photo.Id)
                    .subscribe(res => {
                        _removeResult.Succeeded = res.Succeeded;
                        _removeResult.Message = res.Message;
                    },
                    error => console.error('Error: ' + error),
                    () => {
                        if (_removeResult.Succeeded) {
                            this.notificationService.printSuccessMessage(photo.Title + ' удалена из галереи.');
                            this.getAlbumPhotos();
                        }
                        else {
                            this.notificationService.printErrorMessage('Не удалось удалить фотографию.');
                        }
                    });
            });
    }

    public hasBaseDropZoneOver: boolean = false;
    public hasAnotherDropZoneOver: boolean = false;

    public fileOverBase(e: any): void {
        this.hasBaseDropZoneOver = e;
    }

    public fileOverAnother(e: any): void {
        this.hasAnotherDropZoneOver = e;
    }

    public selectFile($event): void {
        var inputValue = $event.target;
        if (null == inputValue || null == inputValue.files[0]) {
            console.debug("Input file error.");
            return;
        } else {
            this._file = inputValue.files[0];
            console.debug("Input File name: " + this._file.name + " type:" + this._file.size + " size:" + this._file.size);
        }
    }
}