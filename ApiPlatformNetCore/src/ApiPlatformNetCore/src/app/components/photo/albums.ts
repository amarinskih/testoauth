﻿import { Component, OnInit } from '@angular/core';
import { Router, RouterLink } from '@angular/router';

import { Album } from '../../core/domain/album';
import { Paginated } from '../../core/common/paginated';
import { ApplicationRequestService } from '../../../core/services/applicationRequestService';
import { UtilityService } from '../../core/services/utilityService';
import { NotificationService } from '../../core/services/notificationService';
import { photoRoutes } from './photo.routes';

@Component({
    selector: 'albums',
    templateUrl: './albums.html'
})
export class Albums extends Paginated implements OnInit {
    private _albumsAPI: string = 'api/albums/';
    private _albums: Array<Album>;
    private routes = photoRoutes;

    constructor(public albumsService: ApplicationRequestService,
        public utilityService: UtilityService,
        public notificationService: NotificationService,
        public router: Router) {
        super(0, 0, 0);
    }

    ngOnInit() {
        this.routes = photoRoutes;
        this.getAlbums();
    }

    getAlbums(): void {

        this.albumsService.get("/api/photo/get/0/10").subscribe(
            response => {
                let result = this.albumsService.mapResponseModel(response);
            },
            error => {
                if (error.status == 401 || error.status == 404) {
                    this.notificationService.printErrorMessage('Доступ запрещен!!!');
                    this.utilityService.navigateToSignIn();
                }
            }
        );
    }

    search(i): void {
        super.search(i);
        this.getAlbums();
    };

    convertDateTime(date: Date) {
        return this.utilityService.convertDateTime(date);
    }
}