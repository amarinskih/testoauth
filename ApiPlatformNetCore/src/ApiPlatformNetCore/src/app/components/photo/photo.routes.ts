﻿import { ModuleWithProviders } from '@angular/core';
import { Route, RouterModule } from '@angular/router';

import { AlbumPhotos } from './albumPhotos';
import { Albums } from './albums';
import { Photos } from './photos';

export const photoRoutes: { [key: string]: Route } = {
    '': { path: '', redirectTo: 'AlbumPhotos', pathMatch: 'full' },
    'album_photos': { path: 'AlbumPhotos', component: AlbumPhotos },
    'albums': { path: 'Albums', component: Albums },
    'photos': { path: 'Photo', component: Photos },
};

export const appRoutingProviders: any[] = [

];

export const routing: ModuleWithProviders = RouterModule.forChild(Object.keys(photoRoutes).map(key => photoRoutes[key]));