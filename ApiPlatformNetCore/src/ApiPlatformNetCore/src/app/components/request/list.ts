﻿import {Component, OnInit} from '@angular/core';
import { ActivatedRoute }    from '@angular/router';

import { UtilityService } from '../../core/services/utilityService';
import { Paginated } from '../../core/common/paginated';
import { DataService } from '../../core/services/dataService';
import { Request } from '../../core/domain/request';
import { NotificationService } from '../../core/services/notificationService';
import { OperationResult } from '../../core/domain/operationResult';

@Component({
    selector: 'list',
    templateUrl: './list.html'
})
export class List extends Paginated implements OnInit {
    private _requestAPI: string = 'api/request/';
    private _requestId: string;
    private _requests: Array<Request>;
    private _displayingTotal: number;

    constructor(public dataService: DataService,
        public utilityService: UtilityService,
        public notificationService: NotificationService,
        public route: ActivatedRoute) {
        super(0, 0, 0);
    }

    ngOnInit() {
        this.dataService.set(this._requestAPI + "get/", 10);
        this.getRequests();        
    }

    getRequests(): void {
        this.dataService.get(this._page)
            .subscribe(res => {
                var data: any = res.json();
                this._requests = data.Items;
                this._displayingTotal = this._requests.length;
                this._page = data.Page;
                this._pagesCount = data.TotalPages;
                this._totalCount = data.TotalCount;
            },
            error => {
                if (error.status == 401 || error.status == 302) {
                    this.utilityService.navigateToSignIn();
                }

                console.error('Error: ' + error)
            },
            () => console.log(this._requests));
    }

    delete(request: Request) {
        var _removeResult: OperationResult = new OperationResult(false, '');

        this.notificationService.printConfirmationDialog('Вы уверены, что хотите удалить заявку?',
            () => {
                this.dataService.deleteResource(this._requestAPI + "delete/" + request.Id)
                    .subscribe(res => {
                        _removeResult.Succeeded = res.Succeeded;
                        _removeResult.Message = res.Message;
                    },
                    error => console.error('Error: ' + error),
                    () => {
                        if (_removeResult.Succeeded) {
                            this.notificationService.printSuccessMessage(request.FIO + ' удалена.');
                            this.getRequests();
                        }
                        else {
                            this.notificationService.printErrorMessage('Не удалось удалить заявку.');
                        }
                    });
            });
    }
}