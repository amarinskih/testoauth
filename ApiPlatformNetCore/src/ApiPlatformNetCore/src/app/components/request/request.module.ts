﻿import { NgModule } from '@angular/core';
import { CommonModule }  from '@angular/common';
import { RouterModule } from '@angular/router';

import { FILE_UPLOAD_DIRECTIVES } from 'ng2-file-upload'

import { routing } from './request.routes';

import { Edit } from './edit';
import { List } from './list';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        routing
    ],
    declarations: [
        Edit,
        List,
        FILE_UPLOAD_DIRECTIVES
    ],
    providers: [],
})
export class RequestModule { }