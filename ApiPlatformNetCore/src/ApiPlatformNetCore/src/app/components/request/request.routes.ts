﻿import { ModuleWithProviders } from '@angular/core';
import { Route, RouterModule } from '@angular/router';

import { List } from './list';
import { Edit } from './edit';

export const requestRoutes: { [key: string]: Route } = {
    '': { path: '', redirectTo: 'list', pathMatch: 'full' },
    'list': { path: 'list', component: List },
    'edit': { path: 'edit', component: Edit },
};

export const appRoutingProviders: any[] = [

];

export const routing: ModuleWithProviders = RouterModule.forChild(Object.keys(requestRoutes).map(key => requestRoutes[key]));