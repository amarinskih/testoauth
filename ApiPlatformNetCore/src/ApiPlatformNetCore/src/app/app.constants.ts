﻿
export class EndPoints {
    public static baseUrl: string = "/api";
    public static login: string = "/account/login";
    public static logout: string = "/account/logout";
    public static register: string = "/account/register";
    public static confirmEmail: string = "/account/confirmEmail";
    public static recoveryPassword: string = "/account/recoverypassword";
    public static resetPassword: string = "/account/newpassword";
    public static token: string = "/token";
}

export class Constants {
    public static client_id: string =  "js_app";
}