﻿export class Request {
    Id: number;
    FIO: string;
    Phone: string;
    Comment: string;
    Amount: number;
    Description: string;
    DateCreated: Date;
    House: string;
    Flat: string;
    StatusRequest: string;

    constructor(id: number,
        fio: string,
        phone: string,
        comment: string,
        amount: number,
        description: string,
        dateCreated: Date,
        house: string,
        flat: string,
        statusRequest: string) {
        this.Id = id;
        this.FIO = fio;
        this.Phone = phone;
        this.Comment = comment;
        this.Amount = amount;
        this.Description = description;
        this.DateCreated = dateCreated;
        this.House = house;
        this.Flat = flat;
        this.StatusRequest = statusRequest;
    }
}