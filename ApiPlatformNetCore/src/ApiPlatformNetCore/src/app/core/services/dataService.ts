﻿import { Http, Response, RequestOptionsArgs, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DataService {

    public _pageSize: number;
    public _baseUri: string;

    constructor(public http: Http) {
        http['_defaultOptions']['body'] = "";
    }

    set(baseUri: string, pageSize?: number): void {
        this._baseUri = baseUri;
        this._pageSize = pageSize;
    }

    get(page: number, requestOptions?: RequestOptionsArgs) {
        var customOptions = this.applyAuthHeaders(requestOptions);
        var uri = this._baseUri + page.toString() + '/' + this._pageSize.toString();

        return this.http.get(uri, customOptions)
            .map(response => (<Response>response));
    }

    post(data?: any, mapJson: boolean = true, requestOptions?: RequestOptionsArgs): Observable<any> {
        var customOptions = this.applyAuthHeaders(requestOptions);
        if (mapJson)
            return this.http.post(this._baseUri, data, requestOptions)
                .map(response => <any>(<Response>response).json());
        else
            return this.http.post(this._baseUri, data, customOptions);
    }

    delete(id: number, requestOptions?: RequestOptionsArgs) {
        var customOptions = this.applyAuthHeaders(requestOptions);
        return this.http.delete(this._baseUri + '/' + id.toString(), customOptions)
            .map(response => <any>(<Response>response).json());
    }

    deleteResource(resource: string, requestOptions?: RequestOptionsArgs) {
        var customOptions = this.applyAuthHeaders(requestOptions);
        return this.http.delete(resource)
            .map(response => <any>(<Response>response).json(), customOptions);
    }

    applyAuthHeaders(requestOptions?: RequestOptionsArgs) {
        requestOptions = requestOptions || {};
        if (requestOptions.headers != null)
            requestOptions.headers.append("authorization", "Bearer " + window.sessionStorage["access_token"]);
        else
            requestOptions.headers = new Headers({ "authorization": "Bearer " + window.sessionStorage["access_token"] });
        return requestOptions;
    }

    urlEncode(obj: Object) {
        var str = [];
        for (var p in obj)
            if (obj.hasOwnProperty(p)) {
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
        return str.join("&");
    }
}