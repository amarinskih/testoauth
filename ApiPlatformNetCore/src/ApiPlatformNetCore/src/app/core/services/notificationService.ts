﻿import { Injectable } from '@angular/core';
import { OperationResult } from '../../core/domain/operationResult';

@Injectable()
export class NotificationService {
    private alerty: alertify.IAlertifyStatic;
    constructor() {
        this.alerty.set({ delay: 1000, labels: { ok: "OK", cancel: "Отмена" }, buttonFocus: "ok", buttonReverse: true  });
    }

    printSuccessMessage(message: string) {
        
        this.alerty.success(message);
    }

    printErrorMessage(message: string) {
        this.alerty.error(message);
    }

    printConfirmationDialog(message: string, okCallback: () => any) {
        this.alerty.confirm(message, function (e) {
            if (e) {
                okCallback();
            } else {
            }
        });
    }

    printMessage(operationResult: OperationResult) {
        if (operationResult.Succeeded) {
            this.printSuccessMessage(operationResult.Message);
        }
        else {
            this.printErrorMessage(operationResult.Message);
        }
    }
}