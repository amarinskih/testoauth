﻿import { Constants, EndPoints } from '../../app.constants.ts';

import { Http, Response, Request, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ApplicationRequestService, UserStoreService, User, ContentType, ResponseModel, ResponseResultModel } from '../../../core/core.module';

import { DataService } from './dataService';

import { LoginModel, RegisterModel, RegisterResponse, ConfirmEmailRequest, RestorePasswordModel } from '../../components/account/account.models';


@Injectable()
export class AccountService {

    private _accountRegisterAPI: string = EndPoints.baseUrl + EndPoints.register;
    private _accountLoginAPI: string = EndPoints.baseUrl + EndPoints.login;
    private _accountTokenAPI: string = EndPoints.baseUrl + EndPoints.token;
    private _accountLogoutAPI: string = EndPoints.baseUrl + EndPoints.logout;
    private _accountConfirmEmailAPI: string = EndPoints.baseUrl + EndPoints.confirmEmail;
    private _accountRecoverytPasswordAPI: string = EndPoints.baseUrl + EndPoints.recoveryPassword;
    private _accountResetPasswordAPI: string = EndPoints.baseUrl + EndPoints.resetPassword;




    constructor(
        public requetService: ApplicationRequestService,
        private _userStorage: UserStoreService) {

    }

    register(newUser: RegisterModel): Observable<ResponseResultModel<RegisterResponse>> {
        return this.requetService.post(this._accountRegisterAPI, newUser).map(response => {
            let result = this.requetService.mapResultResponseModel<RegisterResponse>(response);
            if (result.Succeeded && !result.Result.NeedConfirmation) {
                this._userStorage.setUser(result.Result.User);
            }
            return result;
        });

    }

    confirmEmail(userId: string, code: string): Observable<ResponseResultModel<User>> {
        return this.requetService.post(this._accountConfirmEmailAPI, new ConfirmEmailRequest(userId, code)).map(response => {
            let result = this.requetService.mapResultResponseModel<User>(response);
            if (result.Succeeded) {
                this._userStorage.setUser(result.Result);
            }
            return result;
         });
    }

    login(creds: LoginModel): Observable<ResponseResultModel<User>> {
        return this.requetService.post(this._accountLoginAPI, creds, ContentType.JSON).map(response => {
            let result = this.requetService.mapResultResponseModel<User>(response);
            if (result.Succeeded) {
                this._userStorage.setUser(result.Result);
            }
            return result;
        });
    }

    logout(): Observable<ResponseModel>{
        return this.requetService.get(this._accountLogoutAPI).map(response => {
            let result = this.requetService.mapResponseModel(response);
            if (result.Succeeded) {
                this._userStorage.resetUser();
            }
            return result; 
        });

    }

    recoveryPassword(confirmEmail: string): Observable<ResponseModel> {
        return this.requetService.post(this._accountRecoverytPasswordAPI, { Email: confirmEmail }).map(response => {
            return this.requetService.mapResponseModel(response);
        })
    }

    resetPassword(confirmPassword: RestorePasswordModel): Observable<ResponseModel> {
        return this.requetService.post(this._accountResetPasswordAPI, confirmPassword).map(response => {
            return this.requetService.mapResponseModel(response);
        })
    }



    refreshToken(): Observable<ResponseResultModel<User>> {
        let user = this._userStorage.getUser();
        if (user) {
            let request = {
                grant_type: 'refresh_token',
                client_id: Constants.client_id,
                refresh_token: user.RefreshToken
            };
            return this.requetService.post(this._accountTokenAPI, request, ContentType.FormUrlEncoded).map(response => {
                let result = <LoginResponse>response;
                var user = this._userStorage.getUser();
                if (!!user && !!result.access_token) {
                    user.AccessToken = result.access_token;
                    user.RefreshToken = result.refresh_token;
                }
                return result;
            })
        }
        return null;
    }

    setUser(user: User) {
        this._userStorage.setUser(user);
    }


}

export interface LoginResponse {
    access_token: string;
    refresh_token: string;
    expires_in: number;
}