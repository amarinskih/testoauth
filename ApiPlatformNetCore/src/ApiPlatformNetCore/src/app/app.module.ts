﻿import { NgModule } from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { RouterModule, DefaultUrlSerializer, UrlTree, UrlSerializer } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { CoreModule } from '../core/core.module';


import { AccountService } from './core/services/accountService'; 

import { DataService } from './core/services/dataService';
import { NotificationService } from './core/services/notificationService';
import { UtilityService } from './core/services/utilityService';


import { routing, appRoutingProviders } from './app.routes';

import './components/account/styles/base.scss'
import './components/request/styles/base.scss'
import './Content/app.scss'


import { Home } from './components/home';

export class LowerCaseUrlSerializer extends DefaultUrlSerializer {
    parse(url: string): UrlTree {
        url = url.replace(/\?.*/, '').toLowerCase() + (url.indexOf('?') >= 0 ? url.substring(url.indexOf('?'), url.length) : '');
        return super.parse(url);
    }
}

@NgModule({
    imports: [
        BrowserModule,
        RouterModule,
        HttpModule,
        FormsModule,
        routing,
        CoreModule.forRoot()
    ],
    declarations: [
        AppComponent,
        Home
    ],
    providers: [
        appRoutingProviders,
        DataService,
        NotificationService,
        UtilityService,
        AccountService,
        {
            provide: UrlSerializer,
            useClass: LowerCaseUrlSerializer
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }

