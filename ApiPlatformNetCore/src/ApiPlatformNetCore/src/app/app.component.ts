﻿import { Component, OnInit, enableProdMode } from '@angular/core';
import { Headers, RequestOptions, BaseRequestOptions} from '@angular/http';
import { Routes, Router } from '@angular/router';
import { Location, LocationStrategy, HashLocationStrategy } from '@angular/common';
import 'rxjs/add/operator/map';

import { routing, appRoutes } from './app.routes';

import { UtilityService } from './core/services/utilityService';

import { UserStoreService } from '../core/core.module';
import { AccountService } from './core/services/accountService';

enableProdMode();

@Component({
    selector: 'my-app',
    templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {

    constructor(public accountService: AccountService,
        public userStore: UserStoreService,
        public router: Router
    ) { }

    ngOnInit() {

    }

    isUserLoggedIn(): boolean {
        return !!this.userStore.getUser();
    }

    getUserName(): string {
        if (this.isUserLoggedIn()) {
            var _user = this.userStore.getUser();
            return _user.UserName;
        }
        else
            return 'Account';
    }

    logout(): void {
        this.accountService.logout()
            .subscribe(res => {
                this.router.navigate(['/home']);
            },
            error => console.error('Error: ' + error),
            () => { });
    }
}