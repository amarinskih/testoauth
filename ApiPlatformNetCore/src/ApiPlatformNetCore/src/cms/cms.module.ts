import './rxjs-operators';
import { RouterModule, DefaultUrlSerializer, UrlTree, UrlSerializer} from '@angular/router';
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule }    from '@angular/http';
import { PaginationModule, Ng2BootstrapModule, ModalModule, ProgressbarModule, TimepickerModule, DatepickerModule } from 'ng2-bootstrap/ng2-bootstrap';
import { SlimLoadingBarService, SlimLoadingBarComponent } from 'ng2-slim-loading-bar';
import { CmsComponent }   from './cms.component';
import { DateFormatPipe } from './shared/pipes/date-format.pipe';
import { HighlightDirective } from './shared/directives/highlight.directive';
import { HomeComponent } from './home/home.component';
import { MobileHideDirective } from './shared/directives/mobile-hide.directive';
import { ScheduleEditComponent } from './schedules/schedule-edit.component';
import { ScheduleListComponent } from './schedules/schedule-list.component';
import { routing } from './cms.routes';
import { DataService } from './shared/services/data.service';
import { ConfigService } from './shared/utils/config.service';
import { ItemsService } from './shared/utils/items.service';
import { MappingService } from './shared/utils/mapping.service';
import { NotificationService } from './shared/utils/notification.service';
import { CoreModule } from '../core/core.module';

export class LowerCaseUrlSerializer extends DefaultUrlSerializer {
    parse(url: string): UrlTree {
        url = url.replace(/\?.*/, '').toLowerCase() + (url.indexOf('?')>=0?url.substring(url.indexOf('?'), url.length):'');
        return super.parse(url);
    };
}


@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        Ng2BootstrapModule,
        ModalModule,
        ProgressbarModule,
        PaginationModule,
        routing,
        TimepickerModule,
        DatepickerModule,
        CoreModule.forRoot()
    ],
    declarations: [
        CmsComponent,
        DateFormatPipe,
        HighlightDirective,
        HomeComponent,
        MobileHideDirective,
        ScheduleEditComponent,
        ScheduleListComponent,
        SlimLoadingBarComponent,
    ],
    providers: [
        ConfigService,
        DataService,
        ItemsService,
        MappingService,
        NotificationService,
        SlimLoadingBarService,
        {
            provide: UrlSerializer,
            useClass: LowerCaseUrlSerializer
        }

    ],
    bootstrap: [CmsComponent]
})
export class CmsModule { }
