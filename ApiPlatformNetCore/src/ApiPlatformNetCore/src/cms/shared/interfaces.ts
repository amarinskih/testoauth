﻿import { RequestModel } from '../../core/core.module';

export class BaseEntity implements RequestModel {
    Id: number;
    toDTO() {
        return this;
    }
}

export class User extends BaseEntity {
    Id: number;
    Username: string;
    Email: string;
    Roles: ISelectItem[];
    IsLocked: boolean;
    DateCreated: Date;
    PhoneNumber: string;
    constructor() {
        super();
        this.Id = -1;
        this.Roles = [];
        this.DateCreated = new Date();
    }
}

export interface IRole {
    Id: number;
    Name: string;
}

export class ISelectItem {
    id: string;
    text: string;
}

export class PaginationHeaders {
    /**
     * Текущая страницы.
     */
    CurrentPage: number;
    /**
      * Кол-во строк на странице.
      */
    ItemsPerPage: number;
    /**
     * Сортировка.
     */
    Sorting: Sorting;
    constructor(currentPage: number, itemsPerPage: number) {
        this.CurrentPage = currentPage;
        this.ItemsPerPage = itemsPerPage;
        this.Sorting = new Sorting();
    }
}
export class Sorting {
    ColumnName: string;
    Sort: boolean;
    constructor(columnName?: string, sort?: boolean) {
        this.ColumnName = columnName;
        this.Sort = sort;
    }
}

export interface ISchedule {
    id: number;
    title: string;
    description: string;
    timeStart: Date;
    timeEnd: Date;
    location: string;
    type: string;
    status: string;
    dateCreated: Date;
    dateUpdated: Date;
    creator: string;
    creatorId: number;
    attendees: number[];
}

export interface IScheduleDetails {
    id: number;
    title: string;
    description: string;
    timeStart: Date;
    timeEnd: Date;
    location: string;
    type: string;
    status: string;
    dateCreated: Date;
    dateUpdated: Date;
    creator: string;
    creatorId: number;
    attendees: User[];
    statuses: string[];
    types: string[];
}

export interface Pagination {
    CurrentPage: number;
    ItemsPerPage: number;
    TotalItems: number;
    TotalPages: number;
}

export class PaginatedResult<T> {
    result: T;
    pagination: Pagination;
}

export interface Predicate<T> {
    (item: T): boolean
}

export interface ISetting {
    EmailAdmin: string;
    SmtpUserName: string;
    SmtpPassword: string;
    SmtpHost: string;
    SmtpPort: number;
    SmtpEnableSsl: boolean;
}