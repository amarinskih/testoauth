import {Directive, ElementRef, Output, EventEmitter} from '@angular/core';
import {NgModel} from '@angular/forms';
declare var $: JQueryStatic;
@Directive({
    selector: '[mask]',
    host: {
        '(keyup)': 'onInputChange()'
    }
})
export class MaskDirective {
    @Output() ngModelChange: EventEmitter<any> = new EventEmitter()
    maskElement: any;

    constructor(public el: ElementRef, public model: NgModel) {
        this.maskElement = $(el.nativeElement).inputmask();
    }
    onInputChange() {
        this.ngModelChange.emit(this.maskElement.val());
    }
}