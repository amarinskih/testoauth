import { Injectable } from '@angular/core';
import { Predicate } from '../interfaces'


@Injectable()
export class ItemsService {

    constructor() { }

    /*
    Removes an item from an array using the lodash library
    */
    removeItemFromArray<T>(array: Array<T>, item: any) {
        for (var i in array) {
            if (JSON.stringify(array[i]) === JSON.stringify(item)) {
                array.splice(parseInt(i), 1);
            }
        }
    }

    removeItems<T>(array: Array<T>, predicate: Predicate<T>) {
        for (var i in array) {
            if (predicate(array[i])) {
                array.splice(parseInt(i), 1);
            }
        }
    }

    findAll<T>(array: Array<T>, predicate: Predicate<T>) {
        var filtred: Array<T> = [];
        if (array) {
            for (var i in array) {
                if (predicate(array[i])) {
                    filtred.push(array[i])
                }
            }
            return filtred;
        }
    }

    /*
    Finds a specific item in an array using a predicate and repsaces it
    */
    setItem<T>(array: Array<T>, predicate: Predicate<T>, item: T) {
        var self = this;
        var _oldItem = self.findAll(array, predicate)[0];
        if(_oldItem){
            var index = array.indexOf(_oldItem);
            array.splice(index, 1, item);
        } else {
            array.push(item);
        }
    }

    /*
    Adds an item to zero index
    */
    addItemToStart<T>(array: Array<T>, item: any) {
        array.splice(0, 0, item);
    }

    /*
    From an array of type T, select all values of type R for property
    */
    getPropertyValues<T, R>(array: Array<T>, property : string) : R
    {
        var result = array.map(item => {
            return item[property];
        });
        return <R><any>result;
    }

    /*
    Util method to serialize a string to a specific Type
    */
    getSerialized<T>(arg: any): T {
        return <T>JSON.parse(JSON.stringify(arg));
    }
}
