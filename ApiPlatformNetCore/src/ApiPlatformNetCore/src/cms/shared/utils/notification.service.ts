﻿import { Injectable } from '@angular/core';
import { Predicate } from '../interfaces'


declare var alertify: alertify.IAlertifyStatic;

@Injectable()
export class NotificationService {
    private _notifier: alertify.IAlertifyStatic = alertify;

    constructor() {
        this._notifier.set({ delay: 1000, labels: { ok: "OK", cancel: "Отмена" }, buttonFocus: "ok", buttonReverse: true });
        //debugger;
    }

    /*
    Opens a confirmation dialog using the alertify.js lib
    */
    openConfirmationDialog(message: string, okCallback: () => any) {
        this._notifier.confirm(message, function (e) {
            if (e) {
                okCallback();
            } else {
            }
        });
    }

    /*
    Prints a success message using the alertify.js lib
    */
    printSuccessMessage(message: string) {

        this._notifier.success(message);
    }

    /*
    Prints an error message using the alertify.js lib
    */
    printErrorMessage(message: string) {
        this._notifier.error(message);
    }
}
