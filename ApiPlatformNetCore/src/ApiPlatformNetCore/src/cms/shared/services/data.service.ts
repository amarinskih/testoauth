﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
//Grab everything with import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { BaseEntity, User, ISchedule, IScheduleDetails, Pagination, PaginatedResult, PaginationHeaders } from '../interfaces';
import { ItemsService } from '../utils/items.service';
import { ConfigService } from '../utils/config.service';
import {ApplicationRequestService} from '../../../core/core.module';

@Injectable()
export class DataService {

    _baseUrl: string = '';

    constructor(private requestService: ApplicationRequestService,
        private itemsService: ItemsService,
        private configService: ConfigService) {
        this._baseUrl = configService.getApiURI();
    }

    getItems<T>(url: string): Observable<T[]> {
        return this.requestService.get(this._baseUrl + url)
            .map((res: Response) => {
                return res.json();
            })
            .catch(this.handleError);
    }

    getItem<T>(url: string): Observable<T> {
        return this.requestService.get(this._baseUrl + url)
            .map((res: Response) => {
                return res.json();
            })
            .catch(this.handleError);
    }

    getPagesItems<T>(url: string, paginationHeaders?: PaginationHeaders): Observable<PaginatedResult<T[]>> {
        var paginatedResult: PaginatedResult<T[]> = new PaginatedResult<T[]>();
        let headers = new Headers();
        paginationHeaders.ItemsPerPage
        if (paginationHeaders != null) {
            headers.append('Pagination', JSON.stringify(paginationHeaders));
        }
        return this.requestService.get(this._baseUrl + url, null, { headers: headers })
            .map((res: Response) => {
                paginatedResult.result = res.json();
                if (res.headers.get("Pagination") != null) {
                    var paginationHeader: Pagination = this.itemsService.getSerialized<Pagination>(JSON.parse(res.headers.get("Pagination")));
                    paginatedResult.pagination = paginationHeader;
                }
                return paginatedResult;
            })
            .catch(this.handleError);
    }

    getUsers(): Observable<User[]> {
        return this.requestService.get(this._baseUrl + 'users')
            .map((res: Response) => {
                return res.json();
            })
            .catch(this.handleError);
    }

    getUserDetails(id: number): Observable<User> {
        return this.requestService.get(this._baseUrl + 'users/' + id + '/details')
            .map((res: Response) => {
                return res.json();
            })
            .catch(this.handleError);
    }

    getUserSchedules(id: number): Observable<ISchedule[]> {
        return this.requestService.get(this._baseUrl + 'users/' + id + '/schedules')
            .map((res: Response) => {
                return res.json();
            })
            .catch(this.handleError);
    }

    createObject(url: string, obj: BaseEntity): Observable<User> {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this.requestService.post(this._baseUrl + url, obj)
            .map((res: Response) => {
                return res.json();
            })
            .catch(this.handleError);
    }

    updateObject(url: string, obj: BaseEntity): Observable<void> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.requestService.put(this._baseUrl + url, obj)
            .map((res: Response) => {
                return;
            })
            .catch(this.handleError);
    }

    deleteObject(url:string , id: number): Observable<void> {
        return this.requestService.delete(this._baseUrl + url + id)
            .map((res: Response) => {
                return;
            })
            .catch(this.handleError);
    }
    /*
    getSchedules(page?: number, itemsPerPage?: number): Observable<ISchedule[]> {
        let headers = new Headers();
        if (page != null && itemsPerPage != null) {
            headers.append('Pagination', page + ',' + itemsPerPage);
        }

        return this.http.get(this._baseUrl + 'schedules', {
            headers: headers
        })
            .map((res: Response) => {
                return res.json();
            })
            .catch(this.handleError);
    }
    */

    getSchedules(page?: number, itemsPerPage?: number): Observable<PaginatedResult<ISchedule[]>> {
        var peginatedResult: PaginatedResult<ISchedule[]> = new PaginatedResult<ISchedule[]>();

        let headers = new Headers();
        if (page != null && itemsPerPage != null) {
            headers.append('Pagination', page + ',' + itemsPerPage);
        }

        return this.requestService.get(this._baseUrl + 'schedules', {
            headers: headers
        })
            .map((res: Response) => {
                console.log(res.headers.keys());
                peginatedResult.result = res.json();

                if (res.headers.get("Pagination") != null) {
                    //var pagination = JSON.parse(res.headers.get("Pagination"));
                    var paginationHeader: Pagination = this.itemsService.getSerialized<Pagination>(JSON.parse(res.headers.get("Pagination")));
                    console.log(paginationHeader);
                    peginatedResult.pagination = paginationHeader;
                }
                return peginatedResult;
            })
            .catch(this.handleError);
    }

    getSchedule(id: number): Observable<ISchedule> {
        return this.requestService.get(this._baseUrl + 'schedules/' + id)
            .map((res: Response) => {
                return res.json();
            })
            .catch(this.handleError);
    }

    getScheduleDetails(id: number): Observable<IScheduleDetails> {
        return this.requestService.get(this._baseUrl + 'schedules/' + id + '/details')
            .map((res: Response) => {
                return res.json();
            })
            .catch(this.handleError);
    }

    //updateSchedule(schedule: ISchedule): Observable<void> {

    //    let headers = new Headers();
    //    headers.append('Content-Type', 'application/json');

    //    return this.requestService.put(this._baseUrl + 'schedules/' + schedule.id, schedule)
    //        .map((res: Response) => {
    //            return;
    //        })
    //        .catch(this.handleError);
    //}

    deleteSchedule(id: number): Observable<void> {
        return this.requestService.delete(this._baseUrl + 'schedules/' + id)
            .map((res: Response) => {
                return;
            })
            .catch(this.handleError);
    }

    deleteScheduleAttendee(id: number, attendee: number) {

        return this.requestService.delete(this._baseUrl + 'schedules/' + id + '/removeattendee/' + attendee)
            .map((res: Response) => {
                return;
            })
            .catch(this.handleError);
    }

    private handleError(error: any) {
        var applicationError = error.headers.get('Application-Error');
        var serverError = error.json();
        var modelStateErrors: string = '';

        if (!serverError.type) {
            console.log(serverError);
            for (var key in serverError) {
                if (serverError[key])
                    modelStateErrors += serverError[key] + '\n';
            }
        }

        modelStateErrors = modelStateErrors = '' ? null : modelStateErrors;

        return Observable.throw(applicationError || modelStateErrors || 'Server error');
    }
}