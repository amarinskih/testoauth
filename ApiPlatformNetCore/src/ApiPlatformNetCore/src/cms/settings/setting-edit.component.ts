﻿import { Component, OnInit, Directive,
    trigger,
    state,
    style,
    animate,
    transition} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { DataService } from '../shared/services/data.service';
import { ItemsService } from '../shared/utils/items.service';
import { NotificationService } from '../shared/utils/notification.service';
import { ConfigService } from '../shared/utils/config.service';
import { MappingService } from '../shared/utils/mapping.service';
import { ISetting } from '../shared/interfaces';
import { DateFormatPipe } from '../shared/pipes/date-format.pipe';

@Component({
    templateUrl: 'setting-edit.component.html',
    animations: [
        trigger('flyInOut', [
            state('in', style({ opacity: 1, transform: 'translateX(0)' })),
            transition('void => *', [
                style({
                    opacity: 0,
                    transform: 'translateX(-100%)'
                }),
                animate('0.5s ease-in')
            ]),
            transition('* => void', [
                animate('0.2s 10 ease-out', style({
                    opacity: 0,
                    transform: 'translateX(100%)'
                }))
            ])
        ])]
})
export class SettingEditComponent implements OnInit {
    
    apiHost: string;
    id: number;
    settingLoaded: boolean = false;
    statuses: string[];
    rolesId: string[];
    setting: ISetting;
    private sub: any;

    constructor(private route: ActivatedRoute,
        private router: Router,
        private dataService: DataService,
        private itemsService: ItemsService,
        private notificationService: NotificationService,
        private configService: ConfigService,
        private mappingService: MappingService,
        private loadingBarService: SlimLoadingBarService) { }

    ngOnInit() {
        this.apiHost = this.configService.getApiHost();
        this.loadSetting();
    }

    loadSetting() {
        this.dataService.getItem<ISetting>('settings')
            .subscribe((res: ISetting) => {
                this.setting = res;
                this.settingLoaded = true;
            },
            error => {
                if (error.status == 401) {
                    this.notificationService.printErrorMessage('Доступ запрещен!!!');
                    this.router.navigate(['./SomewhereElse']);
                }
            });
    }

    update(editsettingForm: NgForm) {
        if (editsettingForm.valid) {
            this.loadingBarService.start();
            this.dataService.createObject('settings', editsettingForm.value)
                .subscribe(() => {
                    this.notificationService.printSuccessMessage('settings update');
                    this.loadingBarService.complete();
                    this.router.navigate(['/']);
                },
                error => {
                    this.loadingBarService.complete();
                    this.notificationService.printErrorMessage('Failed to add user. ' + error);
                });
        }
    }

    back() {
        this.router.navigate(['/']);
    }

}