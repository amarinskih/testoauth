import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { PaginationModule, Ng2BootstrapModule, ModalModule, ProgressbarModule, TimepickerModule, DatepickerModule } from 'ng2-bootstrap/ng2-bootstrap';
import { routing } from './setting.routes';
import { SettingEditComponent } from './setting-edit.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        HttpModule,
        RouterModule,
        routing,
        PaginationModule
    ],
    declarations: [
        SettingEditComponent
    ],
    providers: []
})
export class SettingModule { }
