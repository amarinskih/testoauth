import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SettingEditComponent } from './setting-edit.component';

export const SettingRoutes:  Routes   = [
    { path: '', component: SettingEditComponent,  },
];
export const routing: ModuleWithProviders = RouterModule.forChild(SettingRoutes);