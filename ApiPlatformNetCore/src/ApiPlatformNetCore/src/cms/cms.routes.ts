import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { UserListComponent } from './users/user-list.component';
import { ScheduleListComponent } from './schedules/schedule-list.component';
import { ScheduleEditComponent } from './schedules/schedule-edit.component';



const cmsRoutes: Routes = [
    //{ path: 'schedules', component: ScheduleListComponent },
    //{ path: 'schedules/:id/edit', component: ScheduleEditComponent },
    { path: '', component: HomeComponent },
    {
        path: 'users',
        loadChildren: () => new Promise(resolve => {
            (require as any).ensure([], (require: any) => {
                resolve(require('./users/user.module').UserModule);
            })
        })
    },
    {
        path: 'settings',
        loadChildren: () => new Promise(resolve => {
            (require as any).ensure([], (require: any) => {
                resolve(require('./settings/setting.module').SettingModule);
            })
        })
    },
    { path: '**', component: HomeComponent },

];

export const routing: ModuleWithProviders = RouterModule.forRoot(cmsRoutes);