﻿import { Component, OnInit, Directive } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { DataService } from '../shared/services/data.service';
import { ItemsService } from '../shared/utils/items.service';
import { NotificationService } from '../shared/utils/notification.service';
import { ConfigService } from '../shared/utils/config.service';
import { MappingService } from '../shared/utils/mapping.service';
import { User, ISelectItem } from '../shared/interfaces';
import { DateFormatPipe } from '../shared/pipes/date-format.pipe';

@Component({
    //moduleId: module.id,
    selector: 'app-user-edit',
    templateUrl: 'user-edit.component.html'
})
export class UserEditComponent implements OnInit {
    
    apiHost: string;
    id: number;
    user: User;
    userLoaded: boolean = false;
    statuses: string[];
    rolesId: string[];
    roles: any[];
    private sub: any;

    constructor(private route: ActivatedRoute,
        private router: Router,
        private dataService: DataService,
        private itemsService: ItemsService,
        private notificationService: NotificationService,
        private configService: ConfigService,
        private mappingService: MappingService,
        private loadingBarService: SlimLoadingBarService) { }

    ngOnInit() {
        // (+) converts string 'id' to a number
        this.id = +this.route.snapshot.params['id'];
        this.apiHost = this.configService.getApiHost();
        this.loadRoles();
        if (this.id > 0)
            this.loaduserDetails();
        else this.newUser();
    }

    loadRoles() {
        this.dataService.getItems<ISelectItem>('roles')
            .subscribe((res: ISelectItem[]) => {
                this.roles = res;

            },
            error => {
                if (error.status == 401) {
                    this.notificationService.printErrorMessage('Доступ запрещен!!!');
                    this.router.navigate(['./SomewhereElse']);
                }
            });
    }

    loaduserDetails() {
        this.loadingBarService.start();
        this.dataService.getUserDetails(this.id)
            .subscribe((user: User) => {
                this.user = this.itemsService.getSerialized<User>(user);
                this.userLoaded = true;
                // Convert date times to readable format
                //this.user.DateCreated = new Date(this.user.DateCreated.toString()); // new DateFormatPipe().transform(this.user.DateCreated, ['local']);
                this.loadingBarService.complete();
            },
            error => {
                this.loadingBarService.complete();
                this.notificationService.printErrorMessage('Failed to load user. ' + error);
            });
    }

    newUser() {
        this.user = new User();
        this.userLoaded = true;
    }

    updateuser(edituserForm: NgForm) {
        if (edituserForm.valid) {
            this.loadingBarService.start();
            if (this.user.Id > 0)
                this.dataService.updateObject('users/' + this.user.Id, edituserForm.value)
                    .subscribe(() => {
                        this.notificationService.printSuccessMessage('user has been updated');
                        this.loadingBarService.complete();
                        this.router.navigate(['/users']);
                    },
                    error => {
                        this.loadingBarService.complete();
                        this.notificationService.printErrorMessage('Failed to update user. ' + error);
                    });
            else this.dataService.createObject('users', edituserForm.value)
                .subscribe(() => {
                    this.notificationService.printSuccessMessage('user has been add');
                    this.loadingBarService.complete();
                    this.router.navigate(['/users']);
                },
                error => {
                    this.loadingBarService.complete();
                    this.notificationService.printErrorMessage('Failed to add user. ' + error);
                });
        }
        console.log(edituserForm);
    }

    back() {
        this.router.navigate(['/users']);
    }

}