import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserListComponent } from './user-list.component';
import { UserEditComponent } from './user-edit.component';

export const UserRoutes:  Routes   = [
    { path: '', component: UserListComponent  },
    { path: ':id/edit', component: UserEditComponent },
    { path: 'add', component: UserEditComponent }
];
export const routing: ModuleWithProviders = RouterModule.forChild(UserRoutes);