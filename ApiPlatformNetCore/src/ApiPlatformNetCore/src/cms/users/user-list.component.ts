﻿import { Component, OnInit,
    trigger,
    state,
    style,
    animate,
    transition} from '@angular/core';
import { DataService } from '../shared/services/data.service';
import { ItemsService } from '../shared/utils/items.service';
import { NotificationService } from '../shared/utils/notification.service';
import { User, PaginatedResult, IRole, PaginationHeaders, Sorting, ISelectItem  } from '../shared/interfaces';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';


@Component({
    //moduleId: module.id,
    selector: 'users',
    templateUrl: 'user-list.component.html',
    animations: [
        trigger('flyInOut', [
            state('in', style({ opacity: 1, transform: 'translateX(0)' })),
            transition('void => *', [
                style({
                    opacity: 0,
                    transform: 'translateX(-100%)'
                }),
                animate('0.5s ease-in')
            ]),
            transition('* => void', [
                animate('0.2s 10 ease-out', style({
                    opacity: 0,
                    transform: 'translateX(100%)'
                }))
            ])
        ])]
})
export class UserListComponent implements OnInit {

    users: User[];
    roles: ISelectItem[];
    addingUser: boolean = false;
    public paginationHeaders: PaginationHeaders = new PaginationHeaders(1, 4);
    public rows: Array<any> = [];
    public columns: Array<any> = [
        { title: 'Id', name: 'Id', sort: false },
        { title: 'Username', name: 'Username', sort: 'asc' },
        { title: 'Email', name: 'Email', sort: '' },
        { title: '', name: 'button', sort: false },
    ];
    public page: number = 1;
    public maxSize: number = 5;
    public numPages: number = 1;
    public length: number = 0;
    public config: any = {
        paging: true,
        sorting: { columns: this.columns },
        //filtering: { filterString: '', columnName: 'Username' }
    };

    public changePage(page: any, data: Array<any> = this.rows): Array<any> {
        let start = (page.page - 1) * page.itemsPerPage;
        let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
        return data.slice(start, end);
    }

    public changeSort(config: any): any {
        this.paginationHeaders.Sorting = null;
        if (!config.sorting) {
            return;
        }
        let columns = this.config.sorting.columns || [];
        let columnName: string = void 0;
        let sort: string = void 0;
        for (let i = 0; i < columns.length; i++) {
            if (columns[i].sort !== '') {
                columnName = columns[i].name;
                sort = columns[i].sort;
            }
        }
        if (!columnName || !sort) {
            return;
        }
        this.paginationHeaders.Sorting = new Sorting(columnName, sort === 'desc' ? false : sort === 'asc' ? true : false);
        // simple sorting

    }

    public changeFilter(data: any, config: any): any {
        if (!config.filtering) {
            return data;
        }

        let filteredData: Array<any> = data.filter((item: any) =>
            item[config.filtering.columnName].match(this.config.filtering.filterString));

        return filteredData;
    }

    public onChangeTable(config: any, page: any = { page: this.paginationHeaders.CurrentPage, itemsPerPage: this.paginationHeaders.ItemsPerPage }): any {
        this.paginationHeaders.CurrentPage = page.page;
        this.paginationHeaders.ItemsPerPage = page.itemsPerPage;
        this.loadingBarService.start();
        if (config.filtering) {
            Object.assign(this.config.filtering, config.filtering);
        }
        if (config.sorting) {
            Object.assign(this.config.sorting, config.sorting);
        }

        let filteredData = this.changeFilter(this.rows, this.config);
        this.changeSort(this.config);

        this.dataService.getPagesItems('users', this.paginationHeaders)
            .subscribe((res: PaginatedResult<User[]>) => {
                this.paginationHeaders.CurrentPage = res.pagination.CurrentPage;
                this.rows = res.result;
                this.length = res.pagination.TotalItems;
                this.loadingBarService.complete();
            },
            error => {
                this.loadingBarService.complete();
                this.notificationService.printErrorMessage('Failed to load users. ' + error);
            });
        //this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
        //this.length = sortedData.length;
    }
    //--------------------------------------


    constructor(private dataService: DataService,
        private itemsService: ItemsService,
        private notificationService: NotificationService,
        private loadingBarService: SlimLoadingBarService) { }




    ngOnInit() {
        this.onChangeTable(this.config);
    }

    //removeUser(user: any) {
    //    var _user: IUser = this.itemsService.getSerialized<IUser>(user.value);
    //    this.itemsService.removeItemFromArray<IUser>(this.users, _user);
    //    // inform user
    //    this.notificationService.printSuccessMessage(_user.Username + ' has been removed');
    //}

    pageChanged(event: any): void {
        this.paginationHeaders.CurrentPage = event.page;
    };

    userCreated(user: any) {
        var _user: User = this.itemsService.getSerialized<User>(user.value);
        this.addingUser = false;
        // inform user
        this.notificationService.printSuccessMessage(_user.Username + ' has been created');
        this.itemsService.setItem<User>(this.users, (u) => u.Id == -1, _user);
        // todo fix user with id:-1
    }

    addUser() {
        this.addingUser = true;
        var newUser = { Id: -1, Username: '', Email: '', Roles: [] };
        this.itemsService.addItemToStart<User>(this.users, newUser);
        //this.users.splice(0, 0, newUser);
    }

    cancelAddUser() {
        this.addingUser = false;
        this.itemsService.removeItems<User>(this.users, x => x.Id < 0);
    }

    removeUser(user: User) {
        this.notificationService.openConfirmationDialog('Вы действительно хотите удалить ' + user.Username +' ?',
            () => {
                this.loadingBarService.start();
                this.dataService.deleteObject('users/', user.Id)
                    .subscribe(() => {
                        this.onChangeTable(this.config);
                        this.notificationService.printSuccessMessage(user.Username + ' will not attend the user.');
                        this.loadingBarService.complete();
                    },
                    error => {
                        this.loadingBarService.complete();
                        this.notificationService.printErrorMessage('Failed to remove ' + user.Username + ' ' + error);
                    });
            });
    }
}