
import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { RouterModule} from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { PaginationModule, Ng2BootstrapModule, ModalModule, ProgressbarModule, TimepickerModule, DatepickerModule } from 'ng2-bootstrap/ng2-bootstrap';
import { routing } from './user.routes';
import { UserEditComponent } from './user-edit.component';
import { UserListComponent } from './user-list.component';
import { MaskDirective } from '../shared/directives/mask.directive';
import 'jquery.inputmask/dist/jquery.inputmask.bundle';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        HttpModule,
        RouterModule,
        routing,
        PaginationModule
    ],
    declarations: [
        UserListComponent,
        UserEditComponent,
        MaskDirective
    ],
    providers: [  
    ],
})
export class UserModule { }
