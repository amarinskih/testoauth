﻿import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { CmsModule } from './cms.module';
import 'bootstrap/dist/css/bootstrap.css';
import './Content/css/start.css';
import './Content/css/cms.scss';
import '../../node_modules/components-font-awesome/css/font-awesome.css';

platformBrowserDynamic().bootstrapModule(CmsModule);