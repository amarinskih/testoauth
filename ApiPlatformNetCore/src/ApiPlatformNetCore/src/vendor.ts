﻿// Angular 2
import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/core';
import '@angular/common';
import '@angular/http';
import '@angular/forms';
import '@angular/router';
// RxJS
import 'rxjs';

//Angular plugins
import 'ng2-file-upload';

//JS Plugins
//import 'alertify/lib/alertify.js';
import 'fancybox';

//CSS plugins
//import './Content/normalize2.scss';
import './Content/general.scss';
import 'alertify/themes/alertify.default.css';
import 'alertify/themes/alertify.core.css';
import 'components-font-awesome/fonts/FontAwesome.otf';
import 'components-font-awesome/fonts/fontawesome-webfont.eot';
import 'components-font-awesome/fonts/fontawesome-webfont.svg';
import 'components-font-awesome/fonts/fontawesome-webfont.ttf';
import 'components-font-awesome/fonts/fontawesome-webfont.woff';
import 'components-font-awesome/fonts/fontawesome-webfont.woff2';