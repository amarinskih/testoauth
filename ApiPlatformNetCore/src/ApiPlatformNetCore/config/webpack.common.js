﻿var webpack = require('webpack');
var path = require("path");
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var helpers = require('./helpers');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var ENV = process.env.ENV = process.env.NODE_ENV = 'test';

module.exports = {
    entry: {
        'polyfills': './src/polyfills.ts',
        'cms-polyfills': './src/polyfills.ts',
        'vendor': './src/vendor.ts',
        'cms-vendor': './src/vendor.ts',
        'app': './src/app/main.ts',
        'cms': './src/cms/main.ts'
    },
    output: {
        path: __dirname + '/dist',
        filename: "[name].js"
    },
    resolve: {
        extensions: ['', '.js', '.ts']
    },
    module: {
        loaders: [
          {
              test: /\.html$/,
              include: helpers.root('src'),
              loader: 'html'
          },
          {
              test: /\.ts$/,
              loaders: ['ts', 'angular2-template-loader']

          },
          {
              test: /\.css$/,
              loader: ExtractTextPlugin.extract('style', 'css?sourceMap'),
          },
          {
              test: /\.styl$/,
              loader: ExtractTextPlugin.extract(
                    'style',
                    'css!stylus-loader')
          },
          {
              test: /\.(scss|sass)$/,
              loader: ExtractTextPlugin.extract(
                    'style',
                    "css!sass?includePaths[]=./node_modules/compass-mixins/lib")
          },
          {
              test: /\.(png|jpe?g|gif|svg|woff|woff2|otf|ttf|eot|ico)([\?]?.*)$/,
              loader: 'file?name=assets/[name].[hash].[ext]'
          }
        ],
        postLoaders: [
            {
                test: /\.(js|ts)$/,
                include: root('src'),
                loader: 'istanbul-instrumenter-loader',
                exclude: [
                  /\.e2e\.ts$/,
                  /node_modules/
                ]
            }
        ],
     //   noParse: [
     ///zone\.js\/dist\/.+/,
     ///angular2\/bundles\/.+/
     //   ]
    },
    stats: { colors: true, reasons: true },
    debug: false,
    plugins: [
      new webpack.optimize.CommonsChunkPlugin({
          name: "app-common",
          chunks: ['app', 'vendor', 'polyfills'],
          minChunks: 2
      }),
      new webpack.optimize.CommonsChunkPlugin({
          name: "cms-common",
          chunks: ['cms', 'cms-vendor', 'cms-polyfills'],
          minChunks: 2
      }),
      new CopyWebpackPlugin([
          { from: './node_modules/alertify/src/alertify.js', to: 'alertify.js' },
      ]),
      new webpack.ProvidePlugin({
          //'alertify':'alertify/src/alertify',
          $: "jquery",
          jQuery: "jquery",
          //'__awaiter': 'ts-helper/awaiter',
          //'__extends': 'ts-helper/extends',
          //'__param': 'ts-helper/param',
          //'Reflect': 'es7-reflect-metadata/dist/browser'
      }),
      new webpack.DefinePlugin({
          // Environment helpers
          'process.env': {
              'ENV': JSON.stringify(ENV),
              'NODE_ENV': JSON.stringify(ENV)
          },
          'global': 'window',
          // TypeScript helpers
          //'__metadata': 'Reflect.metadata',
          //'__decorate': 'Reflect.decorate'
      })
      //new webpack.DllReferencePlugin({
      //    context: __dirname,
      //    manifest: require('../wwwroot/manifests/vendor-manifest.json')
      //}),
      //new webpack.DllReferencePlugin({
      //    context: __dirname,
      //    manifest: require('../wwwroot/manifests/polyfills-manifest.json')
      //}),
      //new HtmlWebpackPlugin({
      //    template: 'src/index.html'
      //})
    ],
    node: {
        global: 'window',
        progress: false,
        crypto: 'empty',
        module: false,
        clearImmediate: false,
        setImmediate: false
    }
};

function root(args) {
    args = Array.prototype.slice.call(arguments, 0);
    return path.join.apply(path, [__dirname].concat(args));
}

function rootNode(args) {
    args = Array.prototype.slice.call(arguments, 0);
    return root.apply(path, ['node_modules'].concat(args));
}