﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPlatformNetCore.ViewModels
{
    public class ConfirmEmailRequest
    {
        public int UserId { get; set; }
        public string Code { get; set; }
    }
}
