﻿using System.ComponentModel.DataAnnotations;

namespace ApiPlatformNetCore.ViewModels
{
    public class RegistrationRequest
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
