﻿using ApiPlatformNetCore.Api.Dto.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPlatformNetCore.Api.Dto.Response
{
    public class RegistrationResult
    {
        public UserModel User { get; set; }
        public bool NeedConfirmation { get; set; }
    }
}
