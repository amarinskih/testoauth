﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPlatformNetCore.Api.Dto.Entities
{
    public class UserModel
    {
        public string AccessToken { get; set; }
        public string RefreshToken {get; set;}
        public string UserName { get; set; }
        public string Email { get; set; }
    }
}
