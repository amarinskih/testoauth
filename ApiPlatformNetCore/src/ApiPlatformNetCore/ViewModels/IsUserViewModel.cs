﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPlatformNetCore.ViewModels
{
    public class IsUserViewModel
    {
        public string Username { get; set; }
    }
}
