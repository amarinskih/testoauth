﻿namespace ApiPlatformNetCore.ViewModels.Pagination
{
    public class Pagination
    {
        public int CurrentPage { get; set; }
        public int ItemsPerPage { get; set; }
        public int TotalItems { get; set; }
        public int TotalPages { get; set; }
    }

    public class PaginationHeader : Pagination {
        public Sorting Sorting { get; set; }
    }
    public class Sorting {
        public string ColumnName { get; set; }
        public bool Sort { get; set; }
    }
}
