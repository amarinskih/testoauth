﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPlatformNetCore.ViewModels
{
    public class SettingViewModel
    {
        [Required]
        public string EmailAdmin { get; set; }
        [Required]
        public string SmtpUserName { get; set; }
        [Required]
        public string SmtpPassword { get; set; }
        [Required]
        public string SmtpHost { get; set; }
        [Required]
        public int SmtpPort { get; set; }
        public bool SmtpEnableSsl { get; set; }
    }
}
