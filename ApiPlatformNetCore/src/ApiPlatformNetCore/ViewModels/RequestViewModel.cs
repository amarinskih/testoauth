﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPlatformNetCore.ViewModels
{
    public class RequestViewModel
    {
        public int Id { get; set; }
        public string FIO { get; set; }
        public string Phone { get; set; }
        public string Comment { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
        public DateTime DateCreated { get; set; }
        public string House { get; set; }
        public string Flat { get; set; }
        public string StatusRequest { get; set; }
    }
}
