﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPlatformNetCore.ViewModels
{
    public class UserViewModel
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public int[] RolesId { get; set; }
        public DateTime DateCreated { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsLocked { get; set; }
        public string Password { get; set; }
    }
    public class RoleViewModel {
        public int id { get; set; }
        public string text { get; set; }
    }
    public class NewPassword {
        [Range(1, int.MaxValue, ErrorMessage = "Please enter valid integer Number")]
        public int Id { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Token { get; set; }
    }
}
