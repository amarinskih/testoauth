﻿using System;

namespace ApiPlatformNetCore.ViewModels
{
    public class ConfirmEmail
    {
        public int UserId { get; set; }
        public Guid Code { get; set; }
        public string Data { get; set; }
    }
}
