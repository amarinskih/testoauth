﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ApiPlatformNetCore.Infrastructure;
using DataAccess.Repositories;
using DataAccess;
using ApiPlatformNetCore.Infrastructure.Services;
using System.Security.Claims;
using Newtonsoft.Json.Serialization;
using ApiPlatformNetCore.Infrastructure.Filters;
using ApiPlatformNetCore.Infrastructure.Mappings;
using ApiPlatformNetCore.Infrastructure.Authentication;
using ApiPlatformNetCore.Migrations;
using Microsoft.AspNetCore.Http;
using ApiPlatformNetCore.Infrastructure.Services.SiteSettings;

namespace ApiPlatformNetCore
{
    public class Startup
    {
        private static string _applicationPath = string.Empty;
        private static string _contentRootPath = string.Empty;
        public Startup(IHostingEnvironment env)
        {
            _applicationPath = env.WebRootPath;
            _contentRootPath = env.ContentRootPath;
            var builder = new ConfigurationBuilder()
                .SetBasePath(_contentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                // For more details on using the user secret store see http://go.microsoft.com/fwlink/?LinkID=532709
                builder.AddUserSecrets();

                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthServices();
            services.AddCors();
            // Add framework services.
            services.Configure<ApplicationConfiguration>(Configuration.GetSection("ConnectionStrings"));
            services.AddApplicationInsightsTelemetry(Configuration);

            services.AddDbContext<DataContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"), b => b.MigrationsAssembly("ApiPlatformNetCore")));

            services.AddRepositoryServices();
            
            // Services
            services.AddScoped<IMembershipService, MembershipService>();
            services.AddScoped<IEncryptionService, EncryptionService>();

            //services.AddAuthentication(options =>
            //{
            //    options.SignInScheme = "Bearer";
            //});

            //// Polices
            //services.AddAuthorization(options =>
            //{
            //    // inline policies
            //    options.AddPolicy("AdminOnly", policy =>
            //    {
            //        policy.RequireClaim(ClaimTypes.Role, "Admin");
            //    });

            //});

            // Add MVC services to the services container.
            services.AddMvc()
                .AddJsonOptions(opt =>
                {
                    var resolver = opt.SerializerSettings.ContractResolver;
                    if (resolver != null)
                    {
                        var res = resolver as DefaultContractResolver;
                        res.NamingStrategy = null;
                    }
                });


            services.AddScoped<ValidateMimeMultipartContentFilter>();
            services.AddScoped<CustomExceptionFilterAttribute>();
            services.AddScoped<IEmailSender, AuthMessageSender>();
            services.AddSingleton<SiteSettingsService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseDeveloperExceptionPage();
            app.UseOAuthAuthentication();
            // this will serve up wwwroot
            app.UseFileServer();

            AutoMapperConfiguration.Configure();

            // Custom authentication middleware
            //app.UseMiddleware<AuthMiddleware>();

            // Add MVC to the request pipeline.
            app.UseMvc(routes =>
            {
                routes.MapRoute("api", "api/{controller}/{action}/{id?}");
                routes.MapRoute("apiCms", "api/cms/{controller}/{action}/{id?}", new { action = "Get" });
                routes.MapRoute("apiError", "api/{*anything}", new { controller = "Error", action = "NotFoundError" });
                routes.MapRoute(null, "cms/{*anything}", new { controller = "Home", action = "Cms" });
                routes.MapRoute("default", "{*anything}", new { controller = "Home", action = "Index" });
            });
            app.UseStaticFiles();

            DatabaseSeed.Initialize(app.ApplicationServices);
        }
    }
}
