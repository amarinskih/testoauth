﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ApiPlatformNetCore.ViewModels;
using AutoMapper;
using ApiPlatformNetCore.Infrastructure.Core;
using Microsoft.AspNetCore.Authorization;
using System.Net.Http;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using ApiPlatformNetCore.Infrastructure.Filters;
using Microsoft.Net.Http.Headers;
using Microsoft.Extensions.Options;
using ApiPlatformNetCore.Infrastructure;
using DataAccess;
using DataAccess.Repositories;
using DataAccess.Entities;
using DataAccess.Entities.File;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Web.Http;
using NetCore.Infrastructure;
using ApiPlatformNetCore.Infrastructure.Api;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ApiPlatformNetCore.Controllers
{
    [Route("api/[controller]")]
    public class AlbumsController : BaseApiController
    {
        private readonly IAuthorizationService _authorizationService;
        private readonly IOptions<ApplicationConfiguration> _optionsApplicationConfiguration;
        private readonly IHostingEnvironment _hostingEnvironment;
        IAlbumRepository _albumRepository;
        IPhotoRepository _photoRepository;
        ILoggingRepository _loggingRepository;
        IFileRepository _fileRepository;

        public AlbumsController(IAuthorizationService authorizationService,
                                IAlbumRepository albumRepository,
                                ILoggingRepository loggingRepository,
                                IFileRepository fileRepository,
                                IOptions<ApplicationConfiguration> o,
                                IHostingEnvironment hostingEnvironment,
                                IPhotoRepository photoRepository
            )
        {
            _authorizationService = authorizationService;
            _albumRepository = albumRepository;
            _loggingRepository = loggingRepository;
            _fileRepository = fileRepository;
            _optionsApplicationConfiguration = o;
            _hostingEnvironment = hostingEnvironment;
            _photoRepository = photoRepository;
        }

        [HttpGet("{page:int=0}/{pageSize=12}")]
        public async Task<IActionResult> Get(int? page, int? pageSize)
        {
            PaginationSet<AlbumViewModel> pagedSet = new PaginationSet<AlbumViewModel>();

            try
            {
                if (await _authorizationService.AuthorizeAsync(User, "AdminOnly"))
                {
                    int currentPage = page.Value;
                    int currentPageSize = pageSize.Value;

                    List<Album> _albums = null;
                    int _totalAlbums = new int();


                    _albums = _albumRepository
                        .AllIncluding(a => a.Photos)
                        .OrderBy(a => a.Id)
                        .Skip(currentPage * currentPageSize)
                        .Take(currentPageSize)
                        .ToList();

                    _totalAlbums = _albumRepository.GetAll().Count();

                    IEnumerable<AlbumViewModel> _albumsVM = Mapper.Map<IEnumerable<Album>, IEnumerable<AlbumViewModel>>(_albums);

                    pagedSet = new PaginationSet<AlbumViewModel>()
                    {
                        Page = currentPage,
                        TotalCount = _totalAlbums,
                        TotalPages = (int)Math.Ceiling((decimal)_totalAlbums / currentPageSize),
                        Items = _albumsVM
                    };
                }
                else
                {
                    CodeResultStatus _codeResult = new CodeResultStatus(401);
                    return new ObjectResult(_codeResult);
                }
            }
            catch (Exception ex)
            {
                _loggingRepository.Add(new Error() { Message = ex.Message, StackTrace = ex.StackTrace, DateCreated = DateTime.Now });
                _loggingRepository.Commit();
            }

            return new ObjectResult(pagedSet);
        }


        [HttpGet("{id:int}/photos/{page:int=0}/{pageSize=12}")]
        public PaginationSet<PhotoViewModel> Get(int id, int? page, int? pageSize)
        {
            PaginationSet<PhotoViewModel> pagedSet = null;

            try
            {
                int currentPage = page.Value;
                int currentPageSize = pageSize.Value;

                List<Photo> _photos = null;
                int _totalPhotos = new int();

                Album _album = _albumRepository.GetSingle(a => a.Id == id, a => a.Photos);


                _photos = _album
                            .Photos
                            .OrderBy(p => p.Id)
                            .Skip(currentPage * currentPageSize)
                            .Take(currentPageSize)
                            .ToList();

                _totalPhotos = _album.Photos.Count();

                IEnumerable<PhotoViewModel> _photosVM = Mapper.Map<IEnumerable<Photo>, IEnumerable<PhotoViewModel>>(_photos);

                pagedSet = new PaginationSet<PhotoViewModel>()
                {
                    AlbumTitle = _album.Title,
                    Page = currentPage,
                    TotalCount = _totalPhotos,
                    TotalPages = (int)Math.Ceiling((decimal)_totalPhotos / currentPageSize),
                    Items = _photosVM
                };
            }
            catch (Exception ex)
            {
                _loggingRepository.Add(new Error() { Message = ex.Message, StackTrace = ex.StackTrace, DateCreated = DateTime.Now });
                _loggingRepository.Commit();
            }

            return pagedSet;
        }

        [Route("fileuploader")]
        [HttpPost]
        [ServiceFilter(typeof(ValidateMimeMultipartContentFilter))]
        [ServiceFilter(typeof(CustomExceptionFilterAttribute))]
        public async Task<IEnumerable<PhotoViewModel>> UploadFiles(FileDescriptionShort fileDescriptionShort)
        {
            var names = new List<string>();
            var contentTypes = new List<string>();
            if (ModelState.IsValid)
            {
                var al = _albumRepository.GetSingle(a => a.Id == fileDescriptionShort.Id);
                if (al == null)
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NoContent) { ReasonPhrase = "Not Album" });
                string dir = Path.Combine(_hostingEnvironment.WebRootPath, _optionsApplicationConfiguration.Value.ServerUploadFolder);
                Directory.CreateDirectory(dir);
                List<Photo> _photos = new List<Photo>();
                foreach (var file in fileDescriptionShort.File)
                {
                    if (file.Length > 0)
                    {
                        var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                        contentTypes.Add(file.ContentType);
                        names.Add(fileName);
                        _photos.Add(new Photo { AlbumId = al.Id, Title = fileName, DateUploaded = DateTime.UtcNow, Uri = fileName, Album = al });
                        try
                        {
                            await file.SaveAsAsync(Path.Combine(dir, fileName));
                        }
                        catch (Exception e)
                        {
                            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError) { ReasonPhrase = e.Message });
                        }
                    }
                }
                //var files = new DataAccess.Entities.File.FileResult
                //{
                //    FileNames = names,
                //    ContentTypes = contentTypes,
                //    Description = fileDescriptionShort.Description,
                //    CreatedTimestamp = DateTime.UtcNow,
                //    UpdatedTimestamp = DateTime.UtcNow,
                //};
                //var _photo = new Photo { AlbumId = al.Id, Title = files.FileNames[0], DateUploaded = DateTime.UtcNow, Uri = files.FileNames[0], Album = al };
                _photoRepository.AddRange(_photos);
                //List<FileDescription> _files;
                //_fileRepository.AddFileDescriptions(files, out _files);
                return Mapper.Map<IEnumerable<Photo>, IEnumerable<PhotoViewModel>>(_photos);
            }
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = "Error model" });
        }

        [Route("download/{id}")]
        [HttpGet]
        public FileStreamResult Download(int id)
        {
            var fileDescription = _fileRepository.GetFileDescription(id);

            var path = Path.Combine(_hostingEnvironment.WebRootPath, _optionsApplicationConfiguration.Value.ServerUploadFolder) + "\\" + fileDescription.FileName;
            var stream = new FileStream(path, FileMode.Open);
            return File(stream, fileDescription.ContentType);
        }

    }
}
