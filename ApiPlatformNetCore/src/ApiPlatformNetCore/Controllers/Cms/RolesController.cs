using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ApiPlatformNetCore.ViewModels;
using AutoMapper;
using DataAccess.Entities;
using DataAccess.Repositories;
using Microsoft.AspNetCore.Authorization;
using ApiPlatformNetCore.Infrastructure.Filters;
using ApiPlatformNetCore.Controllers.Cms.Abstract;

namespace ApiPlatformNetCore.Controllers.Cms
{
   
    public class RolesController : BaseCmsApiController
    {
        private readonly IRoleRepository _roleRepository;
        public RolesController(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }
        // GET: api/Role
        [HttpGet]

        public async Task<List<RoleViewModel>> Get()
        {
            List<RoleViewModel> listRole = null;
                var _roles = await _roleRepository.GetAllAsync();
                listRole = Mapper.Map<List<Role>, List<RoleViewModel>>(_roles.ToList());
            return listRole;
        }

        // GET: api/Role/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }
        
        // POST: api/Role
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }
        
        // PUT: api/Role/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
