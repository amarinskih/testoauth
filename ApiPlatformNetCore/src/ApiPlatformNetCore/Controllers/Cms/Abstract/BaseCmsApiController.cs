﻿using ApiPlatformNetCore.Infrastructure.Api;
using ApiPlatformNetCore.Infrastructure.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPlatformNetCore.Controllers.Cms.Abstract
{
    [Produces("application/json")]
    [Route("api/cms/[controller]")]
    [Authorize(Roles = "users")]
    //[ServiceFilter(typeof(ValidateMimeMultipartContentFilter))]
    //[ServiceFilter(typeof(CustomExceptionFilterAttribute))]
    //[Authorize(Policy = "AdminOnly")]
    public abstract class BaseCmsApiController : BaseApiController
    {
        protected IActionResult ResultAction(bool isError, string message)
        {
            IActionResult _result = new ObjectResult(false);
            GenericResult _genericResult = new GenericResult()
            {
                Succeeded = isError,
                Message = message
            };
            _result = new ObjectResult(_genericResult);
            return _result;
        }

    }
}
