﻿using ApiPlatformNetCore.Infrastructure.Services;
using ApiPlatformNetCore.ViewModels;
using DataAccess.Entities;
using DataAccess.Repositories;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiPlatformNetCore.Infrastructure.Core;
using ApiPlatformNetCore.Controllers.Cms.Abstract;
using ApiPlatformNetCore.Infrastructure.Services.SiteSettings;

namespace ApiPlatformNetCore.Controllers.Cms
{
    public class SettingsController : BaseCmsApiController
    {
        private readonly ISettingRepository _settingRepository;
        
        SiteSettingsService _siteSettingsService { get; set; }

        public SettingsController(SiteSettingsService siteSettingsService, ISettingRepository settingRepository)
        {
            _siteSettingsService = siteSettingsService;
            _settingRepository = settingRepository;
        }

        public SettingViewModel Get()
        {
            return _siteSettingsService.SiteSettings;
        }
        [HttpPost]
        public IActionResult Post([FromBody]SettingViewModel model)
        {
            if (!ModelState.IsValid) return ResultAction(false, "Invalid fields.");
            try
            {
                string valueJson = JsonConvert.SerializeObject(model);
                _settingRepository.UpdateOptions(valueJson);
                _siteSettingsService.RefreshSettings(_settingRepository);
               
            }
            catch (Exception ex)
            {
                return ResultAction(false, ex.Message);
            }
            return ResultAction(false, "Update settings"); ;
        }

    }
}
