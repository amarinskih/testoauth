using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DataAccess.Repositories;
using ApiPlatformNetCore.ViewModels;
using Microsoft.Extensions.Primitives;
using AutoMapper;
using DataAccess.Entities;
using Newtonsoft.Json;
using ApiPlatformNetCore.ViewModels.Pagination;
using ApiPlatformNetCore.Infrastructure.Core;
using ApiPlatformNetCore.Infrastructure.Services;
using ApiPlatformNetCore.Infrastructure.Extensions;
using Microsoft.AspNetCore.Authorization;
using ApiPlatformNetCore.Controllers.Cms.Abstract;
using Microsoft.AspNetCore.Identity;

namespace ApiPlatformNetCore.Controllers.Cms
{
    public class UsersController : BaseCmsApiController
    {
        private readonly IMembershipService _membershipService;
        private readonly IUserRepository _userRepository;
        private readonly UserManager<User> _userManager;

        public UsersController(UserManager<User> userManager,  IMembershipService membershipService, IUserRepository userRepository)
        {
            _userManager = userManager;
            _membershipService = membershipService;
            _userRepository = userRepository;
        }
        
         public async Task<List<UserViewModel>> Get()
        {
            List<UserViewModel> listUser = null;
            var _users = await _userRepository.GetUserRolesAsync();
            int _totalItems = _users.Count();
            StringValues _sv;
            if (Request.Headers.TryGetValue("Pagination", out _sv))
            {
                var p = JsonConvert.DeserializeObject<PaginationHeader>(_sv.ToString());
                if (p.Sorting != null)
                {
                    _users = _users.OrderBy(p.Sorting.ColumnName, p.Sorting.Sort);
                }
                p.TotalItems = _totalItems;
                if ((p.CurrentPage - 1) * p.ItemsPerPage >= p.TotalItems) { p.CurrentPage = p.TotalItems / p.ItemsPerPage; }
                Response.Headers["Pagination"] = JsonConvert.SerializeObject(p as Pagination);
                listUser = Mapper.Map<List<User>, List<UserViewModel>>(_users.ToList().Skip((p.CurrentPage - 1) * p.ItemsPerPage).Take(p.ItemsPerPage).ToList());
            }
            else
            {
                listUser = Mapper.Map<List<User>, List<UserViewModel>>(_users.OrderBy(e => e.Id).ToList());
            };
            return listUser;
        }

        [HttpGet("{id}/Details")]
        public async Task<UserViewModel> Details(int id)
        {
            var _users = await _userRepository.GetUserRolesAsync();
            return Mapper.Map<User, UserViewModel>(_users.FirstOrDefault(e => e.Id == id));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] UserViewModel model)
        {
            if (!ModelState.IsValid) return ResultAction(false, "Invalid fields.");
            try
            {
                User _user = _userRepository.GetSingle(id);
                Mapper.Map(model, _user);
                _userRepository.Edit(_user);
                _userRepository.Commit();
                _userRepository.UpdateRoles(_user.Id, model.RolesId);
                if (!string.IsNullOrEmpty(model.Password)) {
                    var result = await _userManager.AddPasswordAsync(_user, model.Password);
                    if (!result.Succeeded)
                    {
                        ModelState.AddModelError("Password", "Password not walid.");
                        return Error(ModelState);
                    }
                }
            }
            catch (Exception ex)
            {
                return ResultAction(false, ex.Message);
            }
            return ResultAction(true, "Registration succeeded.");
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]UserViewModel model)
        {
            if (!ModelState.IsValid) return ResultAction(false, "Invalid fields.");
            try
            {
                var user = Mapper.Map<User>(model);
                await _userManager.CreateAsync(user, "Qwerty*1");
                _userRepository.Add(user);
                _userRepository.UpdateRoles(user.Id, model.RolesId);
            }
            catch (Exception ex)
            {
                return ResultAction(false, ex.Message);
            }

            return ResultAction(false, "Add user"); ;
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            User _user = _userRepository.GetSingle(id);
            _userRepository.Delete(_user);
            _userRepository.Commit();
            return ResultAction(true, "Delete user");
        }

    }
}