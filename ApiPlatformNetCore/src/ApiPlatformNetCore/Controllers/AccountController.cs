﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ApiPlatformNetCore.ViewModels;
using System.Security.Claims;
using ApiPlatformNetCore.Api.Abstract;
using DataAccess.Entities;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;
using AspNet.Security.OpenIdConnect.Server;
using AspNet.Security.OpenIdConnect.Extensions;
using ApiPlatformNetCore.Infrastructure.Api;
using ApiPlatformNetCore.Infrastructure.Authentication.Services;
using ApiPlatformNetCore.Api.Dto.Entities;
using ApiPlatformNetCore.Infrastructure.Services;
using Microsoft.AspNetCore.Identity;
using ApiPlatformNetCore.Api.Dto.Response;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ApiPlatformNetCore.Controllers
{
    [Route("api/account")]
    public class AccountController : BaseApiController
    {
        private readonly IAuthenticationService _authService;
        private readonly IEmailSender _emailSender;
        private readonly UserManager<User> _userManager;
        private readonly AuthenticationOption _authOptions = new AuthenticationOption();

        public AccountController(UserManager<User> userManager, IAuthenticationService authService, IEmailSender emailSender)
        {
            _userManager = userManager;
            _authService = authService;
            _emailSender = emailSender;
        }

        [Authorize(Roles = "users")]
        [HttpGet("userinfo")]
        public IActionResult Get()
        {
            return Json(new
            {
                sub = User.GetClaim(ClaimTypes.NameIdentifier),
                name = User.GetClaim(ClaimTypes.Name)
            });
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login([JsonPostBodyModelBinder] LoginViewModel model)
        {
            try
            {
                var authresult = await _authService.Authorize(model.Username, model.Password);
                if (!authresult.Succeeded)
                {
                    return BadRequest(new ApiError { Message = authresult.Reason.Message });
                }
                if (authresult.IsTwoFactor)
                {
                    //TODO реализовать логику для двух-факторной авторизации 
                }
                var ticket = await _authService.GetAuthentiticationTicket(authresult.User, OpenIdConnectServerDefaults.AuthenticationScheme);
                await HttpContext.Authentication.SignInAsync(ticket.AuthenticationScheme, ticket.Principal, ticket.Properties);

                var tokens = _authService.SerializeTiket(ticket);
                var responseModel = new UserModel
                {
                    AccessToken = tokens.AccessToken,
                    RefreshToken = tokens.RefreshToken,
                    UserName = authresult.User.UserName,
                    Email = authresult.User.Email
                };
                return Success(responseModel);
            }
            catch (Exception ex)
            {
                return Error(ex);
            }
        }

        [HttpGet("logout")]
        [AllowAnonymous]
        public async Task<IActionResult> Logout()
        {
            try
            {
               // await _authService.SignOut();
                return Success();
            }
            catch (Exception ex)
            {
                return Error(ex);
            }
        }

        [HttpPost("register")]
        [AllowAnonymous]
        public async Task<IActionResult> Register([JsonPostBodyModelBinder] RegistrationRequest model)
        {
            try
            {
                var response = new RegistrationResult();
                if (ModelState.IsValid)
                {
                    var user = await _authService.GetUserByEmail(model.Email);
                    if (user != null) {
                        if (_authOptions.NeedEmailConfirmation && !user.EmailConfirmed)
                        {
                            await SendRegistrationEmail(user);
                            response.NeedConfirmation = true;
                            return Success(response);
                        }
                        else {
                            return Error(new ApiError { Message = "Пользователь с данным email уже существует" });
                        }
                    }

                    user = Mapper.Map<User>(model);
                    var registerResault = await _authService.RegistrateUser(user, model.Password);
                    if (registerResault.Succeeded)
                    {

                        if (_authOptions.NeedEmailConfirmation) {
                            await SendRegistrationEmail(user);
                            response.NeedConfirmation = true;
                            return Success(response);
                        }

                        var ticket = await _authService.GetAuthentiticationTicket(registerResault.User, OpenIdConnectServerDefaults.AuthenticationScheme);
                        var tokens = _authService.SerializeTiket(ticket);
                        response.User = new UserModel
                        {
                            AccessToken = tokens.AccessToken,
                            RefreshToken = tokens.RefreshToken,
                            UserName = registerResault.User.UserName,
                            Email = registerResault.User.Email
                        };
                        return Success(response);
                    }
                    return Error(registerResault.Reason);
                }

                return Error(ModelState);
            }
            catch (Exception ex)
            {
                return Error(ex);
            }
        }

        private async Task SendRegistrationEmail(User user) {
            var code = await _authService.GetEmailConfirmationCode(user);
            var url = string.Format("{0}://{1}/account/confirmemail?userId={2}&code={3}", Request.Scheme, Request.Host, user.Id, code);
            await _emailSender.SendEmailAsync(user.Email, "", string.Format(@"<a href='{0}'>{0}</a> Перейдите по ссылке, чтобы завершить регистрацию", url), true);
        }

        [HttpPost("recoverypassword")]
        [AllowAnonymous]
        public async Task<IActionResult> RecoveryPassword([JsonPostBodyModelBinder] EmailViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user != null)
                {
                    ModelState.AddModelError("", "Invalid user name.");
                }
                else
                {
                    var t = await _userManager.GeneratePasswordResetTokenAsync(user);
                    await _emailSender.SendEmailAsync(user.Email, "", string.Format(@"<a href=""{0}account/resetpassword/{1}?token={2}"">Сменить пароль</a>", Url.RouteUrl("default", null, "http"), user.Id, t), true);
                    return Success("ok");
                }
            }
            return Error(ModelState);
        }

        [HttpPost("newpassword")]
        [AllowAnonymous]
        public async Task<IActionResult> NewPassword([JsonPostBodyModelBinder] NewPassword model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByIdAsync(model.Id.ToString());
                if (user == null) return Error(ModelState);
                var result = await _userManager.ResetPasswordAsync(user, model.Token, model.Password);
                if (!result.Succeeded)
                {
                    return BadRequest(result.Errors);
                }
                return Success("ok");
            }
            return Error(ModelState);
        }

        [HttpPost("confirmemail")]
        public async Task<IActionResult> ConfirmEmail([JsonPostBodyModelBinder] ConfirmEmailRequest model) {
            if (ModelState.IsValid) {
                var user = await _authService.GetUserById(model.UserId);
                var result = await _authService.ConfirmEmail(user, model.Code);
                var ticket = await _authService.GetAuthentiticationTicket(user, OpenIdConnectServerDefaults.AuthenticationScheme);
                var tokens = _authService.SerializeTiket(ticket);
                if (result.Succeeded) {
                    var response = new UserModel
                    {
                        AccessToken = tokens.AccessToken,
                        RefreshToken = tokens.RefreshToken,
                        UserName = user.UserName,
                        Email = user.Email
                    };
                    return Success(response);
                }
                return Error(result.Reason);
            }
            return Error(ModelState);
        }
    }

    public class AuthenticationOption {
        public bool NeedEmailConfirmation { get; set; } = true;
    }
}
