﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ApiPlatformNetCore.ViewModels;
using AutoMapper;
using ApiPlatformNetCore.Infrastructure.Core;
using DataAccess.Repositories;
using DataAccess.Entities;
using Microsoft.AspNetCore.Authorization;
using ApiPlatformNetCore.Infrastructure.Api;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ApiPlatformNetCore.Controllers
{
    [Route("api/[controller]/[action]")]
    public class RequestController : BaseApiController
    {
        IRequestRepository _requestRepository;
        ILoggingRepository _loggingRepository;
        public RequestController(IRequestRepository requestRepository, ILoggingRepository loggingRepository)
        {
            _requestRepository = requestRepository;
            _loggingRepository = loggingRepository;
        }
        
        [HttpGet("{page:int=0}/{pageSize=10}")]
        public async Task<PaginationSet<RequestViewModel>> Get(int? page, int? pageSize)
        {
            var user = User;
            PaginationSet<RequestViewModel> pagedSet = null;

            try
            {
                int currentPage = page.Value;
                int currentPageSize = pageSize.Value;

                var _requestCollection = await _requestRepository.GetAllAsync();
                int _totalRequest = new int();


                var _requestSkipingTaking = _requestCollection
                    .OrderBy(p => p.Id)
                    .Skip(currentPage * currentPageSize)
                    .Take(currentPageSize)
                    .ToList();

                _totalRequest = _requestCollection.Count();

                IEnumerable<RequestViewModel> _requestVM = Mapper.Map<IEnumerable<Request>, IEnumerable<RequestViewModel>>(_requestSkipingTaking);

                pagedSet = new PaginationSet<RequestViewModel>()
                {
                    Page = currentPage,
                    TotalCount = _totalRequest,
                    TotalPages = (int)Math.Ceiling((decimal)_totalRequest / currentPageSize),
                    Items = _requestVM
                };
            }
            catch (Exception ex)
            {
                _loggingRepository.Add(new Error() { Message = ex.Message, StackTrace = ex.StackTrace, DateCreated = DateTime.Now });
                _loggingRepository.Commit();
            }

            return pagedSet;
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            IActionResult _result = new ObjectResult(false);
            GenericResult _removeResult = null;

            try
            {
                var _requestToRemove = await _requestRepository.GetSingleAsync(id);
                _requestRepository.Delete(_requestToRemove);
                _requestRepository.Commit();

                _removeResult = new GenericResult()
                {
                    Succeeded = true,
                    Message = "Photo removed."
                };
            }
            catch (Exception ex)
            {
                _removeResult = new GenericResult()
                {
                    Succeeded = false,
                    Message = ex.Message
                };

                _loggingRepository.Add(new Error() { Message = ex.Message, StackTrace = ex.StackTrace, DateCreated = DateTime.Now });
                _loggingRepository.Commit();
            }

            _result = new ObjectResult(_removeResult);
            return _result;
        }
    }
}
