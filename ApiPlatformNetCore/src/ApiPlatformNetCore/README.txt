﻿Установка нового плагина
1. Пропиши в package.json название плагина и версию в блоке "dependencies"
2.а Если этот плагин глобальный (используется в различных местах в рамках одной страницы), добавь ссылку на его модуль в файл vendor.ts в блок JS Plugins, стили добавь 
в соответствующий ращдел. Он будет грузится синхронно
2.б Если этот плагин локальный, используется в отдельном блоке-компоненте используй функцию-обертку - 

require.ensure([], function() {
	//загрузка модуля1
	var someFunc = require('module1');
	//таким же образом можно загружать статические ресурсы (изображения, css, шрифты)
	
	//Your code here...
})

тогда плагины будут загружаться асинхронно

3. Если плагин написан на js, надо для него загрузить файлы определения *.d.ts. Для этого в консоли, 
1) перейди в директорию проекта (cd D:/path или D: cd D:/path если надо поменять диск) 
2) выполни комманду typings install {plugin_name} --save --global
3) если npm пакет typings у тебя еще не установлен, то выполни npm install typings -g (может потребовать права администратора, тогда запусти консоль от админа) и повтори шаг 2

Токены
Пример запроса
1) Логин (получение access_token)
POST /api/account/token
header: Content-Type application/x-www-form-urlencoded
BODY:
grant_type: password
client_id: js_app (id клиентского приложения(обязателен))
username: username
password: password

возвращаяет json вида: http://joxi.ru/BA0v8JgsBWbb0m

2) Обновление access_token (использование refresh_token)
POST /api/account/token
header: Content-Type application/x-www-form-urlencoded
BODY:
grant_type: refresh_token
client_id: js_app (id клиентского приложения(обязателен))
refresh_token: refresh_token (который получили в предыдущий раз в (1) или (2) случае)

возвращаяет json вида: http://joxi.ru/BA0v8JgsBWbb0m

3) Авторизированный запрос
Добавляется заголовок authorization со значением 'Bearer ' + [access_token]
Bearer CfDJ8I41vsGXWMZKgcW7A-kMq788ZXvYkvt5B_5kFStUUfG-Wc1oXaNgxeVbfk0K4exqpFqidbALbjfvdkaDqFUJRE38nbpLRpAitGu2AP3NU_nYiW_mH9rVCNL4Ki3vLEl9jC5FMS-yhz4mq8ZPWSyGpFmSmFGDf-IY6_-ZOR_1lRuE4QCpQLeTp2Lgk36E5BRnEPnhjhp5Cs0zK4hL4CWeodgC3d69MhOVXu70CVdDkXmTFkZrJfuLyRO-aCgA4BaOLtr8AtxbOC9RqdXA2DHpihPZ-5JlXnVhoLj1eG7WExcR4BHRQkPj5E9euXbXD3iHUEU25PE9BGBkfv3MYuNFnQBd_LUtYDmggBIwp84SGsV6DbDHnyxcrdPEcnvGwMCSuYF3yjD7BTzj3_IOhGrB0rib5D3K58NODFFHnzihgYssVh1jFmRT9oaQKyO64NZabDThb7aETuBjvFsKRENBg52-j7Ii59URmElNPZJz4ddh5NVN0aSwKQromJ3qjikEVcB-AJy78PQrE3MuesfPChnOczNa2F0IuFI4iwRjO0g-Ge0S2D2Qoawp579aoBGZWj9NittzFHrSEn3gQHVlMbWhiq36EKfmVh74yJiSkA9DDlA3rARSOCuIkMeldEZJ_2t6g_49TTUMsqHnGvh-_srFyI08XFUMnulfs-ZAhmaA6riauBc_6YoMsTHNoa68WXwGbo4waskjD993O89fFD7LK1XVuBJ5-q9ZRWL5AQF-z16sLXsQW8rd7fuw7HwJcxbX6AGLHDHdTv6QtuF5Z9GjU2nLfUEg6-06pVgJvIhZ