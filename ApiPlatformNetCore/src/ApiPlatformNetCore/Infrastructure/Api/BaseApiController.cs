﻿using System;
using AspNet.Security.OpenIdConnect.Server;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using ApiPlatformNetCore.Infrastructure.Authentication.Models;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ApiPlatformNetCore.Infrastructure.Api
{
    public class BaseApiController : Controller
    {
        protected IActionResult Success<T>(T model)
        {
            return Ok(new BaseResponseModel<T>(model) { Succeeded = true });
        }

        protected IActionResult Success() {
            return Ok(new BaseResponseModel() { Succeeded = true });
        }

        protected IActionResult Error(ApiError error) {
            return BadRequest(new BaseResponseModel { Message = error });
        }

        protected IActionResult Error(string message) {
            return BadRequest(new BaseResponseModel { Message = new ApiError { Message = message } });
        }

        protected IActionResult Error(Reason reason) {
            return BadRequest(new BaseResponseModel { Message = new ApiError { Message = reason?.Message } });
        }

        protected IActionResult Error(Exception exception) {
            return BadRequest(new BaseResponseModel { Message = new ApiError { Message = exception.Message } });
        }

        protected IActionResult Error(ModelStateDictionary modelState) {
            return BadRequest(modelState);
        }
    }
}
