﻿using Newtonsoft.Json;

namespace ApiPlatformNetCore.Infrastructure.Api
{
    public class ApiError
    {
        public string Message { get; set; }

        public ErrorCodes Code { get; set; }
    }

    public enum ErrorCodes
    {
        UnknownError = 0
    }
}
