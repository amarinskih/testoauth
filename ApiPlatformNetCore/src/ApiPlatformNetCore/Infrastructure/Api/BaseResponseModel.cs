﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPlatformNetCore.Infrastructure.Api
{

    public class BaseResponseModel<T> : BaseResponseModel
    {
        public BaseResponseModel(T model) {
            Result = model;
        }
        public T Result { get; set; }
    }

    public class BaseResponseModel
    {
        public bool Succeeded { get; set; }

        public ApiError Message { get; set; }
    }
}
