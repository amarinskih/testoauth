﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace ApiPlatformNetCore.Api.Abstract
{
    public class JsonModelBinder : IModelBinder
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            return Task.Run(() =>
            {

                try
                {
                    var name = bindingContext.ModelName;

                    var val = bindingContext.ValueProvider.GetValue(name);
                    var content = val.ConvertTo<string>();
                    if (content == null)
                    {
                        bindingContext.Result = ModelBindingResult.Success(null);
                    }

                    var serializer = new JsonSerializer();
                    var result = serializer.Deserialize(new JsonTextReader(new StringReader(content)), bindingContext.ModelType);
                    bindingContext.Result = ModelBindingResult.Success(result);
                }
                catch (Exception ex)
                {
                    bindingContext.ModelState.AddModelError("model", ex, bindingContext.ModelMetadata);
                }
            });
        }
    }

    public class JsonPostBodyModelBinder : IModelBinder
    {
        public async Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (bindingContext == null)
                throw new ArgumentNullException("bindingContext");
            try
            {
                var content = "";
                using (var sr = new StreamReader(bindingContext.ActionContext.HttpContext.Request.Body))
                {
                    content = await sr.ReadToEndAsync();
                }

                if (content == null)
                {
                    bindingContext.Result = ModelBindingResult.Success(null);
                    return;
                }

                var serializer = new JsonSerializer();
                var model = serializer.Deserialize(new JsonTextReader(new StringReader(content)), bindingContext.ModelType);
                bindingContext.Result = ModelBindingResult.Success(model);
            }
            catch (Exception ex)
            {
                bindingContext.ModelState.AddModelError("model", ex, bindingContext.ModelMetadata);
            }
        }
    }

    public class StringPostBodyModelBinder : IModelBinder
    {

        public async Task BindModelAsync(ModelBindingContext bindingContext)
        {
            try
            {
                var content = "";
                using (var sr = new StreamReader(bindingContext.ActionContext.HttpContext.Request.Body))
                {
                    content = await sr.ReadToEndAsync();
                }
                if (bindingContext.ModelType != typeof(string))
                {
                    bindingContext.ModelState.AddModelError("model", "Wrong type of model");
                }
                else
                {
                    bindingContext.Result = ModelBindingResult.Success(content);
                }
            }
            catch (Exception ex)
            {
                bindingContext.ModelState.AddModelError("model", ex, bindingContext.ModelMetadata);
            }
        }
    }

    public class JsonPostBodyModelBinderAttribute : ModelBinderAttribute
    {
        public JsonPostBodyModelBinderAttribute()
        {
            BinderType = typeof (JsonPostBodyModelBinder);
        }

    }

    public class StringPostBodyModelBinderAttribute : ModelBinderAttribute
    {
        public StringPostBodyModelBinderAttribute()
        {
            BinderType = typeof (StringPostBodyModelBinder);
        }
    }

    public class JsonModelBinderAttribute : ModelBinderAttribute
    {
        public JsonModelBinderAttribute()
        {
            BinderType = typeof(JsonModelBinder);
        }
    }

}
