﻿namespace ApiPlatformNetCore.Infrastructure
{
    public class ApplicationConfiguration
    {
        public string DefaultConnection { get; set; }

        public string ServerUploadFolder { get; set; }
    }
}
