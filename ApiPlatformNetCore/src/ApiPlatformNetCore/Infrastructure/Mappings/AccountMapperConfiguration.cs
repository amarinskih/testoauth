﻿using System;
using AutoMapper;
using ApiPlatformNetCore.ViewModels;
using DataAccess.Entities;

namespace ApiPlatformNetCore.Infrastructure.Mappings
{
    public class AccountMapperConfiguration : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<RegistrationRequest, User>()
                .IgnoreAllUnmapped()
                .ForMember(q => q.DateCreated, o => o.UseValue(DateTime.Now))
                .ForMember(q => q.UserName, o => o.MapFrom(w => w.Username))
                .ForMember(q => q.Email, o => o.MapFrom(w => w.Email));
        }
    }
}
