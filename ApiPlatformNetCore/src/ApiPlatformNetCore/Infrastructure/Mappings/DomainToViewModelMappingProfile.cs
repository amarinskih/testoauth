﻿using AutoMapper;
using DataAccess.Entities;
using ApiPlatformNetCore.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiPlatformNetCore.Infrastructure.Helpers;

namespace ApiPlatformNetCore.Infrastructure.Mappings
{
    public class DomainToViewModelMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Photo, PhotoViewModel>()
               .ForMember(vm => vm.Uri, map => map.MapFrom(p => "/images/" + p.Uri));

            Mapper.CreateMap<Request, RequestViewModel>()
               .ForMember(vm => vm.StatusRequest, map => map.MapFrom(p => EnumHelper<StatusRequest>.GetDisplayValue(p.StatusRequest)));

            Mapper.CreateMap<User, UserViewModel>()
                .ForMember(e=>e.Password, q=>q.Ignore())
                .ForMember(e=>e.RolesId, map=> map.MapFrom(p=>p.UserRoles.Select(e=>e.RoleId )));
            Mapper.CreateMap<UserViewModel, User>()
                .ForMember(e => e.Id, q => q.Ignore())
                .ForMember(e => e.UserRoles, q => q.Ignore());

            Mapper.CreateMap<Role, RoleViewModel>()
                .ForMember(e=>e.id, map=> map.MapFrom(a => a.Id))
                .ForMember(e => e.text, map => map.MapFrom(a => a.Name));

            Mapper.CreateMap<Album, AlbumViewModel>()
                .ForMember(vm => vm.TotalPhotos, map => map.MapFrom(a => a.Photos.Count))
                .ForMember(vm => vm.Thumbnail, map => 
                    map.MapFrom(a => (a.Photos != null && a.Photos.Count > 0) ?
                    "/images/" + a.Photos.First().Uri :
                    "/images/thumbnail-default.png"));
        }
    }
}
