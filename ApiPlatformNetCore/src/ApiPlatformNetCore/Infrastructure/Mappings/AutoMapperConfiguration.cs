﻿using AutoMapper;

namespace ApiPlatformNetCore.Infrastructure.Mappings
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<DomainToViewModelMappingProfile>();
                x.AddProfile<AccountMapperConfiguration>();
                //x.AddProfile<ViewModelToDomainMappingProfile>();
            });
            //Mapper.AssertConfigurationIsValid();
        }
    }

    public static class AutoMapperConfigurationExtentions
    {
        public static IMappingExpression<TSource, TDestination> IgnoreAllUnmapped<TSource, TDestination>(this IMappingExpression<TSource, TDestination> mapping)
        {
            mapping.ForAllMembers(q => q.Ignore());
            return mapping;
        }
    }
}
