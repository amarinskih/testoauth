﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using AspNet.Security.OpenIdConnect.Extensions;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.AspNetCore.Identity;
using ApiPlatformNetCore.Infrastructure.Authentication.Models;
using DataAccess.Entities;
using System.Linq;
using AspNet.Security.OpenIdConnect.Server;

namespace ApiPlatformNetCore.Infrastructure.Authentication.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private OpenIdConnectServerOptions _openIdOptions;


        public AuthenticationService(UserManager<User> userManager, OpenIdConnectServerOptions openIdOptions, SignInManager<User> signInManager)
        {
            _userManager = userManager;
            _openIdOptions = openIdOptions;
            _signInManager = signInManager;
        }


        public async Task<AuthorizationResult> Authorize(string login, string password)
        {
            var result = new AuthorizationResult();
            var user = await _userManager.FindByNameAsync(login);
            if (user == null)
            {
                result.Reason = new Reason
                {
                    Code = OpenIdConnectConstants.Errors.InvalidGrant,
                    Message = "Invalid credentials."
                };
                return result;
            }

            // Ensure the user is not already locked out.
            if (_userManager.SupportsUserLockout && await _userManager.IsLockedOutAsync(user))
            {
                result.Reason = new Reason
                {
                    Code = OpenIdConnectConstants.Errors.InvalidGrant,
                    Message = "Invalid credentials."
                };
                return result;
            }

            // Ensure the password is valid.
            if (!await _userManager.CheckPasswordAsync(user, password))
            {
                if (_userManager.SupportsUserLockout)
                {
                    await _userManager.AccessFailedAsync(user);
                }

                result.Reason = new Reason
                {
                    Code = OpenIdConnectConstants.Errors.InvalidGrant,
                    Message = "Invalid credentials."
                };
                return result;
            }

            if (_userManager.SupportsUserLockout)
            {
                await _userManager.ResetAccessFailedCountAsync(user);
            }

            // Reject the token request if two-factor authentication has been enabled by the user.
            if (_userManager.SupportsUserTwoFactor && await _userManager.GetTwoFactorEnabledAsync(user))
            {
                result.IsTwoFactor = true;
                return result;
            }
            result.User = user;
            result.Succeeded = true;
            return result;
        }

        public async Task<AuthenticationTicket> GetAuthentiticationTicket(  User user, string authScheme)
        {
            var identity = await GetClaimsIdentity(user, authScheme);

            var authProperties = new AuthenticationProperties
            {
                AllowRefresh = true,
                ExpiresUtc = DateTime.UtcNow.AddDays(10),
                IssuedUtc = DateTime.UtcNow
            };


            // Create a new authentication ticket holding the user identity.
            var ticket = new AuthenticationTicket(
                new ClaimsPrincipal(identity),
                authProperties,
                authScheme);

            // Set the list of scopes granted to the client application.
            ticket.SetScopes(
                /* openid: */ OpenIdConnectConstants.Scopes.OpenId,
                /* email: */ OpenIdConnectConstants.Scopes.Email,
                /* profile: */ OpenIdConnectConstants.Scopes.Profile,
                 /* offline_access: */ OpenIdConnectConstants.Scopes.OfflineAccess
                );

            // Set the resource servers the access token should be issued for.
            ticket.SetResources("resource_server");
            return ticket;
        }

        public SerializedTicket SerializeTiket(AuthenticationTicket tiket)
        {
            var accessToken = _openIdOptions.AccessTokenFormat.Protect(tiket);
            var refreshToken = _openIdOptions.RefreshTokenFormat.Protect(tiket);
            var expiresIn = tiket.Properties.ExpiresUtc?.Second ?? 0;
            return new SerializedTicket
            {
                AccessToken = accessToken,
                RefreshToken = refreshToken,
                ExpiredIn = expiresIn
            };
        }

        public async Task<ClaimsIdentity> GetClaimsIdentity(User user, string authScheme, string scope = null)
        {
            var identity = new ClaimsIdentity(authScheme);
            var userIdentifer = await _userManager.GetUserIdAsync(user);
            identity.AddClaim(ClaimTypes.NameIdentifier, userIdentifer);
            var roles = await _userManager.GetRolesAsync(user);
            foreach (var role in roles)
            {
                identity.AddClaim(ClaimTypes.Role, role, OpenIdConnectConstants.Destinations.AccessToken);
            }
            return identity;
        }

        public async Task<AuthorizationResult> RegistrateUser(User user, string password)
        {
            var createResult = await _userManager.CreateAsync(user, password);
            if (createResult.Succeeded)
            {
                return new AuthorizationResult()
                {
                    Succeeded = true,
                    User = user
                };
            }
            var reason = createResult.Errors.FirstOrDefault();
            var result = new AuthorizationResult();
            if (reason != null)
            {
                result.Reason = new Reason { Code = reason.Code, Message = reason.Description };
            }
            return result;
        }

        public async Task<User> FindByNameAsync(string name)
        {
            return await _userManager.FindByNameAsync(name);
        }

        public async Task<string> GeneratePasswordResetTokenAsync(User user)
        {
            return await _userManager.GeneratePasswordResetTokenAsync(user);
        }

        public async Task SignOut() {
            await _signInManager.SignOutAsync();
        }

        public async Task<OperationResult> ConfirmEmail(User user, string code) {
            var result = await _userManager.ConfirmEmailAsync(user, code);
            if (result.Succeeded) {
                return new OperationResult { Succeeded = true };
            }
            var error = result.Errors.FirstOrDefault();
            return new OperationResult { Reason = new Reason { Message = error?.Description } };
        }

        public async Task<string> GetEmailConfirmationCode(User user) {
            var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            return code;
        }

        public Task<User> GetUserById(int id)
        {
            return _userManager.FindByIdAsync(id.ToString());
        }

        public Task<User> GetUserByEmail(string email) {
            return _userManager.FindByEmailAsync(email);
        }
    }
}
