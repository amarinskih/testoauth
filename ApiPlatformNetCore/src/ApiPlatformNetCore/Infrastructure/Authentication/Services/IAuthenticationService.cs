﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using ApiPlatformNetCore.Infrastructure.Authentication.Models;
using DataAccess.Entities;

namespace ApiPlatformNetCore.Infrastructure.Authentication.Services
{
    public interface IAuthenticationService
    {
        Task<AuthorizationResult> Authorize(string login, string password);
        Task<AuthenticationTicket> GetAuthentiticationTicket(User user, string authScheme);
        Task<ClaimsIdentity> GetClaimsIdentity(User user, string authScheme, string scope = null);
        Task<AuthorizationResult> RegistrateUser(User model, string password);
        SerializedTicket SerializeTiket(AuthenticationTicket tiket);
        Task SignOut();
        Task<string> GetEmailConfirmationCode(User user);
        Task<OperationResult> ConfirmEmail(User user, string code);
        Task<User> GetUserById(int id);
        Task<User> GetUserByEmail(string email);
    }
}
