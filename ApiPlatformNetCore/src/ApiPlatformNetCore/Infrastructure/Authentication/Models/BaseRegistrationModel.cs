﻿namespace ApiPlatformNetCore.Infrastructure.Authentication.Models
{
    public abstract class BaseRegistrationModel
    {
        public abstract string UserName { get; set; }
        public abstract string Password { get; set; }
        public abstract string Email { get; set; }
    }

    public interface IRegistrationModel
    {
        string UserName { get; set; }
        string Password { get; set; }
        string Email { get; set; }
    }
}
