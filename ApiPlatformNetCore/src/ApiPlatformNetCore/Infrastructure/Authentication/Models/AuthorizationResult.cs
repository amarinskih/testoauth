﻿using DataAccess.Entities;

namespace ApiPlatformNetCore.Infrastructure.Authentication.Models
{
    public class AuthorizationResult : OperationResult
    {
        public bool IsTwoFactor { get; set; }
        public User User { get; set; }
        
    }

    public class OperationResult {
        public bool Succeeded { get; set; }
        public Reason Reason { get; set; } = new Reason();
    }

    public class Reason
    {
        public string Code { get; set; }
        public string Message { get; set; }
    }
}
