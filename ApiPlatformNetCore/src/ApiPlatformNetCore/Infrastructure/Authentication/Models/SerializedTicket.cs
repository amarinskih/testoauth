﻿namespace ApiPlatformNetCore.Infrastructure.Authentication.Models
{
    public class SerializedTicket
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public int ExpiredIn { get; set; }
    }
}
