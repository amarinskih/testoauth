﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AspNet.Security.OpenIdConnect.Extensions;
using AspNet.Security.OpenIdConnect.Server;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.Extensions.DependencyInjection;
using ApiPlatformNetCore.Infrastructure.Authentication.Services;
using NetCore.Core.Authentication;

namespace ApiPlatformNetCore.Infrastructure.Authentication.Providers
{
    public class AuthorizationProvider : OpenIdConnectServerProvider
    {
        public AuthorizationProvider()
        {
            
        }

        public override Task MatchEndpoint(MatchEndpointContext context)
        {
            // Note: by default, OpenIdConnectServerHandler only handles authorization requests made to the authorization endpoint.
            // This context handler uses a more relaxed policy that allows extracting authorization requests received at
            // /connect/authorize/accept and /connect/authorize/deny (see AuthorizationController.cs for more information).
            if (context.Options.AuthorizationEndpointPath.HasValue &&
                context.Request.Path.StartsWithSegments(context.Options.AuthorizationEndpointPath))
            {
                context.MatchesAuthorizationEndpoint();
            }

            return Task.FromResult(0);
        }

        public override Task ValidateTokenRequest(ValidateTokenRequestContext context)
        {
            if (!context.Request.IsPasswordGrantType() && !context.Request.IsRefreshTokenGrantType())
            {
                context.Reject(
                    error: OpenIdConnectConstants.Errors.UnsupportedGrantType,
                    description: "Only resource owner password credentials and refresh token " +
                         "are accepted by this authorization server");
                return Task.FromResult(0);
            }
            var clients = context.HttpContext.RequestServices.GetServices<IClientApplication>();
            var client = clients.FirstOrDefault(q => q.ClientId == context.Request.ClientId);
            if (client == null)
            {
                context.Reject(
                   error: OpenIdConnectConstants.Errors.InvalidClient,
                   description: "Invalid client type");
                return Task.FromResult(0);
            }
            context.Skip();

            return Task.FromResult(0);
        }

        public override async Task HandleTokenRequest(HandleTokenRequestContext context)
        {
            try
            {
                var authService = context.HttpContext.RequestServices.GetRequiredService<IAuthenticationService>();
                if (context.Request.IsPasswordGrantType())
                {
                    var authresult = await authService.Authorize(context.Request.Username, context.Request.Password);
                    if (!authresult.Succeeded)
                    {
                        context.Reject(error: authresult.Reason.Code, description: authresult.Reason.Message);
                        return;
                    }
                    if (authresult.IsTwoFactor)
                    {
                        //TODO реализовать логику для двух-факторной авторизации 
                    }
                    var ticket = await authService.GetAuthentiticationTicket(authresult.User, context.Options.AuthenticationScheme);

                    context.Validate(ticket);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            
        }

        public override Task HandleUserinfoRequest(HandleUserinfoRequestContext context)
        {
            context.SkipToNextMiddleware();

            return Task.FromResult(0);
        }
    }
}
