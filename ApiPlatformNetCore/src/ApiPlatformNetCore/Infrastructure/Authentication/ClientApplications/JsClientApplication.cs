﻿using NetCore.Core.Authentication;

namespace ApiPlatformNetCore.Infrastructure.Authentication.ClientApplications
{
    public class JsClientApplication : IClientApplication
    {
        public string ApplicationType { get { return ClientApplicationTypes.JsApplication; } }
        public string ClientId { get { return "js_app"; } }
        public string ClientSecret { get { return ""; } }
        public ClientSecurityTypes SecurityType { get { return ClientSecurityTypes.Public; } }
    }
}
