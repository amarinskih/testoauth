﻿namespace NetCore.Core.Authentication
{
    public interface IClientApplication
    {
        string ApplicationType { get; }
        string ClientId { get; }
        string ClientSecret { get; }
        ClientSecurityTypes SecurityType { get; }
    }

    public static class ClientApplicationTypes
    {
        public const string JsApplication = "internet";
        public const string Mobile = "mobile";
    }

    public enum ClientSecurityTypes
    {
        /// <summary>
        /// Мобильные и js приложения
        /// </summary>
        Public = 0,
        /// <summary>
        /// Серверные приложения
        /// </summary>
        Private = 1
    }
}
