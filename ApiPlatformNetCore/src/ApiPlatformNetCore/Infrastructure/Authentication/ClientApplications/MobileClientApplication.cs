﻿using NetCore.Core.Authentication;
namespace ApiPlatformNetCore.Infrastructure.Authentication.ClientApplications
{
    public class MobileClientApplication : IClientApplication
    {
        public string ApplicationType { get { return ClientApplicationTypes.Mobile; } }
        public string ClientId { get { return "mobile_app"; } }
        public string ClientSecret { get { return ""; } }
        public ClientSecurityTypes SecurityType { get { return ClientSecurityTypes.Public; } }
    }
}
