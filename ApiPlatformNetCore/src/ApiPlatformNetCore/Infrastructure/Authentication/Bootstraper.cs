﻿using System;
using AspNet.Security.OpenIdConnect.Server;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using ApiPlatformNetCore.Infrastructure.Authentication.ClientApplications;
using ApiPlatformNetCore.Infrastructure.Authentication.Providers;
using ApiPlatformNetCore.Infrastructure.Authentication.Services;
using NetCore.Core.Authentication;
using DataAccess;
using DataAccess.Entities;
using System.Diagnostics;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace ApiPlatformNetCore.Infrastructure.Authentication
{
    public static class Bootstraper
    {
        public static void AddAuthServices(this IServiceCollection services)
        {
            services.AddIdentity<User, Role>().AddEntityFrameworkStores<DataContext, int>().AddDefaultTokenProviders();
            services.AddScoped<IAuthenticationService, AuthenticationService>();
            services.AddAuthentication();
            services.AddOpenIdSettings();
            services.AddSingleton<IClientApplication, JsClientApplication>();
            services.AddSingleton<IClientApplication, MobileClientApplication>();
        }

        private static void AddOpenIdSettings(this IServiceCollection services) {
            var options = new OpenIdConnectServerOptions();
            options.Provider = new AuthorizationProvider();
            options.UserinfoEndpointPath = "/api/account/userinfo";
            options.TokenEndpointPath = "/api/account/token";
            options.ApplicationCanDisplayErrors = false;
            options.LogoutEndpointPath = "/api/account/logout";
            options.AllowInsecureHttp = true;
            options.AutomaticAuthenticate = true;
            services.AddSingleton(options);
        }

        public static void UseOAuthAuthentication(this IApplicationBuilder app)
        {
            var options = app.ApplicationServices.GetRequiredService<OpenIdConnectServerOptions>();

            app.UseOAuthValidation();
            app.UseOpenIdConnectServer(options);
            app.Use(async (context, next) =>
            {
                var t = context.GetOpenIdConnectRequest();
                if (context.Response.StatusCode == 400) {
                    Debug.WriteLine("Intercepted");
                }
                await next();
            });
        }

        public static IdentityBuilder CustomAddIdentity<TUser, TRole>(this IServiceCollection services, Action<IdentityOptions> setupAction = null) where TUser : class where TRole : class
        {
            //AuthenticationServiceCollectionExtensions.AddAuthentication(services, (Action<SharedAuthenticationOptions>)(options => options.SignInScheme = new IdentityCookieOptions().ExternalCookieAuthenticationScheme));
            ServiceCollectionDescriptorExtensions.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>(services);
            ServiceCollectionDescriptorExtensions.TryAddSingleton<IdentityMarkerService>(services);
            ServiceCollectionDescriptorExtensions.TryAddScoped<IUserValidator<TUser>, UserValidator<TUser>>(services);
            ServiceCollectionDescriptorExtensions.TryAddScoped<IPasswordValidator<TUser>, PasswordValidator<TUser>>(services);
            ServiceCollectionDescriptorExtensions.TryAddScoped<IPasswordHasher<TUser>, PasswordHasher<TUser>>(services);
            ServiceCollectionDescriptorExtensions.TryAddScoped<ILookupNormalizer, UpperInvariantLookupNormalizer>(services);
            ServiceCollectionDescriptorExtensions.TryAddScoped<IRoleValidator<TRole>, RoleValidator<TRole>>(services);
            ServiceCollectionDescriptorExtensions.TryAddScoped<IdentityErrorDescriber>(services);
            ServiceCollectionDescriptorExtensions.TryAddScoped<ISecurityStampValidator, SecurityStampValidator<TUser>>(services);
            ServiceCollectionDescriptorExtensions.TryAddScoped<IUserClaimsPrincipalFactory<TUser>, UserClaimsPrincipalFactory<TUser, TRole>>(services);
            ServiceCollectionDescriptorExtensions.TryAddScoped<UserManager<TUser>, UserManager<TUser>>(services);
            ServiceCollectionDescriptorExtensions.TryAddScoped<SignInManager<TUser>, SignInManager<TUser>>(services);
            ServiceCollectionDescriptorExtensions.TryAddScoped<RoleManager<TRole>, RoleManager<TRole>>(services);
            if (setupAction != null)
                OptionsServiceCollectionExtensions.Configure<IdentityOptions>(services, setupAction);
            return new IdentityBuilder(typeof(TUser), typeof(TRole), services);
        }
    }
}
