﻿using DataAccess.Entities;
using System.Security.Principal;

namespace ApiPlatformNetCore.Infrastructure
{
    public class MembershipContext
    {
        public IPrincipal Principal { get; set; }
        public User User { get; set; }
        public bool IsValid()
        {
            return Principal != null;
        }
    }
}
