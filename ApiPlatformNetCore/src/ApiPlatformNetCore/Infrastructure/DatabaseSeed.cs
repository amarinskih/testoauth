﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using DataAccess;
using DataAccess.Entities;
using Microsoft.Extensions.DependencyInjection;
using DataAccess.Repositories;

namespace ApiPlatformNetCore.Migrations
{
    public class DatabaseSeed
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = (DataContext)serviceProvider.GetService(typeof(DataContext));
            SeedUser(serviceProvider);

        }

        private static async void SeedUser(IServiceProvider serviceProvider)
        {
            await Task.Run(async () =>
             {
                 var userManager = serviceProvider.GetRequiredService<UserManager<User>>();
                 var roleManager = serviceProvider.GetRequiredService<RoleManager<Role>>();
                 var userRepository = serviceProvider.GetRequiredService<IUserRepository>();
                 if(userRepository.Any())
                 {
                     return;
                 }

                 const string defaultPassword = "P@ssw0rd";
                 var roles = new List<Role>
             {
                new Role() {Name = "users", NormalizedName = "Пользователи"},
                new Role() {Name = "requests", NormalizedName = "Заявки"},
                new Role() {Name = "settings", NormalizedName = "Настройки"},
             };
                 foreach (var role in roles)
                 {
                     await roleManager.CreateAsync(role);
                 }

                 var admin = new User { NormalizedUserName = "admin", UserName = "administrator" };
                 await userManager.CreateAsync(admin, defaultPassword);

                 foreach (var role in roles)
                 {
                     await userManager.AddToRoleAsync(admin, role.Name);
                 }
             });


            await Task.Run(() =>
             {
                 var requestRepository = serviceProvider.GetRequiredService<IRequestRepository>();
                 if(requestRepository.Any())
                 {
                     return;
                 }
                 var collection = new List<Request>();
                 for (int i = 0; i < 20; i++)
                 {
                     collection.Add(new Request() { Amount = 100, Comment = "Комментарий " + i, DateCreated = new DateTime(2015, 3, 2), FIO = "Тестовое ФИО " + i, Flat = i.ToString(), House = "2" + i, Phone = "8908254814", StatusRequest = StatusRequest.New, Description = "Описание " + i });

                 }
                 requestRepository.AddRange(collection);
                 requestRepository.Commit();
             });


        }
    }
}
