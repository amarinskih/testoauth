﻿using ApiPlatformNetCore.ViewModels;
using DataAccess.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiPlatformNetCore.Infrastructure.Services.SiteSettings
{
    public class SiteSettingsService
    {
        private ISettingRepository _settingRepository { get; set; }
        public SiteSettingsService(ISettingRepository settingRepository)
        {
            _settingRepository = settingRepository;
            if (SiteSettings == null) RefreshSettings();
        }
        public SettingViewModel SiteSettings { get; set; }
        public void RefreshSettings(ISettingRepository settingRepository)
        {
            _settingRepository = settingRepository;
            RefreshSettings();
        }

        private void RefreshSettings()
        {
            var options = _settingRepository.GetOptions();
            var optionsModel = JsonConvert.DeserializeObject<SettingViewModel>(options.Result);
            if (optionsModel == null)
            {
                optionsModel = new SettingViewModel
                {
                    EmailAdmin = "noreply@artsofte.ru",
                    SmtpEnableSsl = false,
                    SmtpHost = "mail.artsofte.ru",
                    SmtpPassword = "noreply100500",
                    SmtpPort = 25,
                    SmtpUserName = "noreply@artsofte.ru"
                };
            }
            SiteSettings = optionsModel;
        }
    }
}
