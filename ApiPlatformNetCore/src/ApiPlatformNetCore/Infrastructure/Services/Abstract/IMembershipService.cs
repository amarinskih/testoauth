﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;

namespace ApiPlatformNetCore.Infrastructure.Services
{
    public interface IMembershipService
    {
        MembershipContext ValidateUser(string username, string password);
        User CreateUser(string username, string email, string password, int[] roles);
        User GetUser(int userId);
        User GetUser(string login);
        List<Role> GetUserRoles(string username);
    }
}
