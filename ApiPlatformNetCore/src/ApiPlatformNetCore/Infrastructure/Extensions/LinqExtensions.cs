﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;


namespace ApiPlatformNetCore.Infrastructure.Extensions
{
    public static class LinqExtensions
    {

        public static IQueryable<TEntity> OrderBy<TEntity>(this IQueryable<TEntity> source, string orderByValues, bool desc) where TEntity : class
        {
            string command = desc ? "OrderBy":"OrderByDescending";
            var type = typeof(TEntity);
            var parameter = Expression.Parameter(type, "p");
            PropertyInfo property;
            MemberExpression propertyAccess;
            property = type.GetProperty(orderByValues);
            propertyAccess = Expression.MakeMemberAccess(parameter, property);
            var orderByExpression = Expression.Lambda(propertyAccess, parameter);
            var resultExpression = Expression.Call(typeof(Queryable), command, new Type[] { type, property.PropertyType },source.Expression, Expression.Quote(orderByExpression));
            return source.Provider.CreateQuery<TEntity>(resultExpression);
        }
    }
}