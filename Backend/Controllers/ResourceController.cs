﻿using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers {
    [Route("api")]
    public class ResourceController : Controller {
        [Authorize, HttpGet]
        [Route("message")]
        public IActionResult GetMessage() {
            var identity = User.Identity as ClaimsIdentity;
            if (identity == null) {
                return BadRequest();
            }

            return Content($"{identity.Name} has been successfully authenticated.");
        }

        [Authorize(Roles = "Test"), HttpGet, Route("message2")]
        public IActionResult GetMessage2()
        {
            var identity = User.Identity as ClaimsIdentity;
            if (identity == null)
            {
                return BadRequest();
            }

            return Content($"{identity.Name} has been successfully authenticated.");
        }

    }
}