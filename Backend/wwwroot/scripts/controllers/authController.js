﻿'use strict';
app.controller('authController', ['$sce', '$scope', '$location', 'authService', '$http', function ($sce, $scope, $location, authService, $http) {

    $scope.message = "";

    $scope.formModel = {
        username: "Bob",
        password: "P@ssw0rd",
        grant_type: "password",
        client_id: "postman"
    }
    $scope.accessToken = "";
    $scope.getToken = function () {
        $http.post("/connect/token", urlEncode($scope.formModel), { headers: { 'Content-Type': "application/x-www-form-urlencoded" } }).then(
            function (response) {
                $scope.accessToken = response.data.access_token;
                console.log(response);
            }, function (error) {
                console.log(error);
            }
        )
    }

    function urlEncode(request) {
        var fields = [];
        for (var key in request) {
            fields.push(encodeURIComponent(key) + "=" + encodeURIComponent(request[key]));
        }
        return fields.join('&');
    }

    $scope.getAuthorized = function () {
        $http.get("/api/message2", { headers: { 'Authorization': "Bearer " + $scope.accessToken } }).then(
            function (response) {
                console.log(response);
            }, function (error) {
                console.log(error);
            }
        )
    }

    $scope.isAuthorized = function () {
        return !!$scope.accessToken;
    }

    $scope.logIn = function () {
        authService.logIn().then(function (response) {
            authService.confirmMessage().then(function (response) { $scope.message = response });
        }, function (err) {
            $scope.message = err;
        });
    };

    $scope.logOut = function () {
        authService.logOut().then(function (response) {
            $scope.message = "";
        }, function (err) {
            $scope.message = err;
        });
    };

    $scope.queryServer = function () {
        authService.confirmMessage().then(function (response) { $scope.message = response });
    };

    $scope.authentication = authService.authentication;
}]);